markdown = '''
<p><a href="https://commonmark.org/help/" rel="nofollow" target="_blank">Markdown</a>
is supported.</p>
'''

markdown_essential = '''
<p><a href="https://commonmark.org/help/" rel="nofollow" target="_blank">Markdown</a> 
font-styles, links and lists are supported.</p>
'''