const activeTabClass = "is-active";

function initMarkdownPreview(fieldName, {msgLoading, msgError, msgEmptyState, isEssential}) {
  const field = document.querySelector(`.js-textarea-${fieldName}`);
  const markdown = document.querySelector(`#markdown-preview-${fieldName}`);
  const tokenInput = document.querySelector(`input[name=csrfmiddlewaretoken]`)
  const tabWrite = document.querySelector(`#tab-write-${fieldName}`);
  const tabPreview = document.querySelector(`#tab-preview-${fieldName}`);
  const write = document.querySelector(`#write-${fieldName}`);
  const preview = document.querySelector(`#preview-${fieldName}`);
  let lastText; // To avoid calling multiple times unchanged text

  function toggleActiveTab() {
    tabWrite.classList.toggle(activeTabClass);
    tabPreview.classList.toggle(activeTabClass);
  }

  function reduceOpacity(text) {
    return `<span class="opacity-75">${text}</span>`;
  }

  function showEmptyState() {
    markdown.innerHTML = reduceOpacity(msgEmptyState);
  }

  function switchTabs(on, off) {
    on.style.display = "block";
    off.style.display = "none";
    toggleActiveTab();
  }

  if (tabPreview && field) {
    tabPreview.addEventListener("click", function (e) {
      e.preventDefault();
      if (!tabPreview.classList.contains(activeTabClass)) {
        switchTabs(preview, write)
        if (field.value !== lastText) {
          markdown.innerHTML = reduceOpacity(msgLoading);
          lastText = field.value;
          if (field.value.trim()) {
            fetch("/markdown/", {
              method: "POST",
              headers: {
                "X-CSRFToken": tokenInput.value,
              },
              body: new URLSearchParams({
                text: field.value,
                is_essential: isEssential,
              }),
            })
              .then((response) => response.json())
              .then((data) => {
                const preview_markdown = data.markdown;
                if (preview_markdown.trim()) {
                  markdown.innerHTML = preview_markdown;
                } else {
                  showEmptyState();
                }
              })
              .catch((err) => {
                lastText = undefined;
                markdown.innerHTML = reduceOpacity(msgError);
              });
          } else {
            showEmptyState();
          }
        }
      }
    });
  }
  if (tabWrite) {
    tabWrite.addEventListener("click", function (e) {
      e.preventDefault();
      if (!tabWrite.classList.contains(activeTabClass)) {
        switchTabs(write, preview)
      }
    });
  }
}
