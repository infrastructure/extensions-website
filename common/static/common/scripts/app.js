(function() {
  // Create function agreeWithTerms
  function agreeWithTerms() {
    const agreeWithTermsTrigger = document.querySelectorAll('.js-agree-with-terms-trigger');
    const agreeWithTermsCheckbox = document.querySelector('.js-agree-with-terms-checkbox');
    const agreeWithTermsFileInput = document.querySelector('.js-submit-form-file-input');
    let agreeWithTermsBtnSubmit = document.querySelector('.js-agree-with-terms-btn-submit');

    if (!agreeWithTermsCheckbox) {
      // Stop function execution if agreeWithTermsCheckbox is not present
      return;
    }

    // Both the file input and checkbox can trigger the submit button.
    agreeWithTermsTrigger.forEach((el) => {
      el.addEventListener('change', function() {

        // Check if checkbox is checked, and file input has a file selected.
        let is_allowed = (agreeWithTermsCheckbox.checked == true) && (agreeWithTermsFileInput.value != "");

        if (is_allowed) {
          agreeWithTermsBtnSubmit.removeAttribute('disabled');
        } else {
          agreeWithTermsBtnSubmit.setAttribute('disabled', true);
        }
      });
    });
  }


  // Create function submitFormFileInputClear
  // When the user chooses a new file to upload, clear the error classes.
  function submitFormFileInputClear() {
    const submitFormFileInput = document.querySelector('.js-submit-form-file-input');

    if (!submitFormFileInput) {
      // Stop function execution if submitFormFileInput is not present
      return;
    }

    submitFormFileInput.addEventListener('change', function(e) {
      e.target.classList.remove('is-invalid');
    });
  }


  // Create function btnBack
  function btnBack() {
    const btnBack = document.querySelectorAll('.js-btn-back');

    btnBack.forEach(function(item) {
      item.addEventListener('click', function(e) {
        e.preventDefault();
        window.history.back();
      });
    });
  }

  // Create function commentForm
  function commentForm() {
    const commentForm = document.querySelector('.js-comment-form');
    if (!commentForm) {
      return;
    }

    const commentFormSelect = commentForm.querySelector('select');
    if (!commentFormSelect) {
      return;
    }

    // Create event comment form select change
    commentFormSelect.addEventListener('change', function(e) {
      let value = e.target.value;
      let verb = 'Comment';
      const activitySubmitButton = document.getElementById('activity-submit');
      activitySubmitButton.classList.remove('btn-primary', 'btn-success', 'btn-warning', 'btn-danger');

      // Hide or show comment form msg on change
      if (value == 'AWC') {
        verb = 'Set as Awaiting Changes';
        activitySubmitButton.classList.add('btn-warning');
      } else if (value == 'AWR') {
        verb = 'Set as Awaiting Review';
      } else if (value == 'APR') {
        verb = 'Approve';
        activitySubmitButton.classList.add('btn-success');
      } else if (value == 'DCL') {
        verb = 'Decline';
        activitySubmitButton.classList.add('btn-danger');
      } else {
        activitySubmitButton.classList.add('btn-primary');
      }

      activitySubmitButton.querySelector('span').textContent = verb;
    });
  }

  // Create function copyInstallUrl
  function copyInstallUrl() {
    function init() {
      // Create variables single
      const btnInstall = document.querySelector('.js-btn-install');
      const btnInstallAction = document.querySelector('.js-btn-install-action');
      const btnInstallGroup = document.querySelector('.js-btn-install-group');

      const dropdownAllVersionsItemDot = document.querySelectorAll('.js-dropdown-all-versions-item-dot');
      const dropdownAllVersionsItemIcon = document.querySelectorAll('.js-dropdown-all-versions-item-icon');
      const dropdownAllVersionsWrapper = document.querySelector('.js-dropdown-all-versions-wrapper');

      // Create variables multiple
      const btnInstallActionItem = document.querySelectorAll('.js-btn-install-action-item');

      // Create function isArchARM
      function isArchARM() {
        // Check if userAgentData and getHighEntropyValues are supported
        if (navigator.userAgentData && navigator.userAgentData.getHighEntropyValues) {
          navigator.userAgentData
            .getHighEntropyValues(["architecture"])
            .then((values) => {
              // Extract the architecture value
              const arch = values.architecture;
              // Check if the architecture is ARM
              if (arch.toLowerCase().includes('arm')) {
                return true;
              } else {
                return false;
              }
            });
        } else {
          // Fallback for browsers that do not support userAgentData or getHighEntropyValues
          return false;
        }
      }

      if (btnInstall == null) {
        return;
      }

      // Hide dropdownAllVersionsWrapper by default
      dropdownAllVersionsWrapper.classList.add('d-none');

      // Show multi OS install buttons based on active platform
      if (btnInstallActionItem.length > 1) {
        // Show dropdownAllVersionsWrapper
        dropdownAllVersionsWrapper.classList.remove('d-none');

        // Get active platform

        /*
        The navigator objects' 'platform' proporties are arbitrary, and specific to the browser.

        For a light match, we can use the first 3 characters of the platform string.
        */

        const activePlatform = navigator.platform.toLowerCase();
        const activePlatformPrefix = activePlatform.substring(0, 3);

        btnInstallActionItem.forEach(function(item) {
          // Hide all items by default
          item.classList.add('d-none');

          // Hide dropdownAllVersionsItemDot by default
          dropdownAllVersionsItemDot.forEach(function(item) {
            item.classList.add('d-none');
          });

          // Style dropdownAllVersionsItemIcon by default
          dropdownAllVersionsItemIcon.forEach(function(item) {
            item.classList.add('text-muted');
          });

          // Get item platform
          const itemPlatform = item.getAttribute('data-platform');
          const itemPlatformBase = itemPlatform.split('-')[0];
          const itemPlatformBasePrefix = itemPlatformBase.substring(0, 3);

          // Show button if item platform matches active platform
          if (itemPlatformBasePrefix.startsWith(activePlatformPrefix)) {
            item.classList.add('btn-install-action-item-active');
            item.classList.remove('d-none');

            // TODO: WIP show only relevant item for active macOS architecture, if architecture detection reliably works
            if (activePlatformPrefix.startsWith('mac')) {
              if (isArchARM()) {

              } else {

              }
            }
          }
        });

        // TODO: refactor and DRY code
        dropdownAllVersionsItemDot.forEach(function(item) {
          // TODO: create named function to not repeat this (optional)
          // Get item platform
          const itemPlatform = item.getAttribute('data-platform');
          const itemPlatformBase = itemPlatform.split('-')[0];
          const itemPlatformBasePrefix = itemPlatformBase.substring(0, 3);

          // Show dropdownAllVersionsItemDot if item platform matches active platform
          if (itemPlatformBasePrefix.startsWith(activePlatformPrefix)) {
            item.classList.remove('d-none');
          }
        });

        dropdownAllVersionsItemIcon.forEach(function(item) {
          // Get item platform
          const itemPlatform = item.getAttribute('data-platform');
          const itemPlatformBase = itemPlatform.split('-')[0];
          const itemPlatformBasePrefix = itemPlatformBase.substring(0, 3);

          // Show dropdownAllVersionsItemIcon if item platform matches active platform
          if (itemPlatformBasePrefix.startsWith(activePlatformPrefix)) {
            item.classList.remove('text-muted');
          }
        });
      }

      // Show btnInstallAction
      btnInstall.addEventListener('click', function() {
        // Hide btnInstallGroup
        btnInstallGroup.classList.add('d-none');

        // Show btnInstallAction
        btnInstallAction.classList.add('show');
      });

      btnInstallActionItem.forEach(function(item) {
        // Create variables in function scope
        const btnInstallDrag = item.querySelector('.js-btn-install-drag');
        const btnInstallDragGroup = item.querySelectorAll('.js-btn-install-drag-group');

        // Get data install URL
        const btnInstallUrl = item.getAttribute('data-install-url');

        // Drag btnInstallUrl
        btnInstallDrag.addEventListener('dragstart', function(e) {
          // Set data install URL to be transferred during drag
          e.dataTransfer.setData('text/plain', btnInstallUrl);

          // Set drag area active
          btnInstallDragGroup.classList.add('opacity-50');
        });

        // Undrag btnInstallUrl
        btnInstallDrag.addEventListener('dragend', function() {
          // Set drag area inactive
          btnInstallDragGroup.classList.remove('opacity-50');
        });
      });
    }

    init();
  }

  // Create function navGlobalLinkSearch
  function navGlobalLinkSearch() {
    const navGlobalLinkSearch = document.querySelector('.js-nav-global-link-search');
    const navGlobalLinkSearchToggle = document.querySelector('.js-nav-global-link-search-toggle');

    // Toggle navbar search on small screens
    navGlobalLinkSearchToggle.addEventListener('click', function() {
      this.classList.toggle('is-active');

      if (this.classList.contains('is-active')) {
        // Show navGlobalLinkSearch
        navGlobalLinkSearch.classList.add('is-active');
      } else {
        navGlobalLinkSearch.classList.remove('is-active');
      }
    });
  }

  // Create function init
  function init() {
    agreeWithTerms();
    submitFormFileInputClear();
    btnBack();
    commentForm();
    copyInstallUrl();
    navGlobalLinkSearch();
  }

  document.addEventListener('DOMContentLoaded', function() {
    init();
  });
}())
