from semantic_version import Version

from django.core.exceptions import ValidationError

msg_invalid = 'Must be a valid version.'


def version(value: str, coerce: bool = False) -> Version:
    try:
        if coerce:
            return Version.coerce(value)
        return Version(value)
    except Exception:
        raise ValidationError(msg_invalid, code='invalid')


def is_same_release(version_1_raw: str, version_2_raw: str) -> bool:
    """Return True if given version strings belong to the same major release"""
    version_1 = Version(version_1_raw)
    version_2 = Version(version_2_raw)
    return version_1.next_minor() == version_2.next_minor()


def is_valid_version_range(version_min: Version = None, version_max: Version = None) -> bool:
    return version_min and version_max and version_max >= version_min


def is_in_version_range(
    version_raw: Version, version_min_raw: Version, version_max_raw: Version = None
) -> bool:
    """Return True if an extension version (min/max) is compatible with a Blender version.

    version_max is excluded, i.e. the check uses a right-open interval [min, max).
    """
    version, version_min = Version(version_raw), Version(version_min_raw)
    version_max = Version(version_max_raw) if version_max_raw is not None else None

    if version < version_min:
        return False

    if version_max is not None and version >= version_max:
        return False

    return True


def migration_semantic_version(version: str) -> str:
    try:
        return str(Version.coerce(version))
    except ValueError:
        return "0.1.0"
