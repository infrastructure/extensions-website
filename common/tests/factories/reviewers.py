from factory.django import DjangoModelFactory

from reviewers.models import ApprovalActivity


class ApprovalActivityFactory(DjangoModelFactory):
    class Meta:
        model = ApprovalActivity
