from factory.django import DjangoModelFactory
import factory
import factory.fuzzy

import apitokens.models


class UserTokenFactory(DjangoModelFactory):
    class Meta:
        model = apitokens.models.UserToken

    token_hash = factory.Faker('lexify', text='fakehash:??????????????????', letters='deadbeef')
