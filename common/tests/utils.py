import io
import itertools
import os
import shutil
import tempfile
from typing import Tuple
import zipfile

import django.urls as urls
from django.utils.functional import cached_property
from django.utils.regex_helper import normalize
import toml

from apitokens.models import UserToken
from common.tests.factories.users import UserFactory
from constants.licenses import LICENSE_GPL3


META_DATA = {
    "id": "an_id",
    "name": "A random add-on",
    "tagline": "An add-on",
    "version": "0.1.0",
    "type": "add-on",
    "license": [LICENSE_GPL3.slug],
    "blender_version_min": "4.2.0",
    "blender_version_max": "4.3.0",
    "schema_version": "1.0.0",
    "maintainer": "",
    "tags": [],
    "website": "https://extensions.blender.org/",
}

try:  # Django 2.0
    url_resolver_types = (urls.URLResolver,)
    DJANGO_2 = True
except AttributeError:  # Django 1.11
    url_resolver_types = (urls.RegexURLResolver,)
    DJANGO_2 = False


class URLEntry:
    def __init__(self, url_bits):
        self.bits = url_bits[:]
        self.name = url_bits[-1].name

    def normalize(self):
        return normalize(self.merged_pattern)

    @cached_property
    def namespace(self):
        return getattr(self.bits[0], 'namespace', None)

    @cached_property
    def qualified_name(self):
        if self.name and self.namespace:
            return '{namespace}:{name}'.format(namespace=self.namespace, name=self.name)
        return self.name

    @cached_property
    def regexes(self):
        if DJANGO_2:
            return [bit.pattern.regex for bit in self.bits]
        else:
            return [bit.regex for bit in self.bits]

    @cached_property
    def merged_pattern(self):
        return ''.join(re.pattern.lstrip('^').rstrip('$') for re in self.regexes)

    @cached_property
    def named_groups(self):
        keys = (re.groupindex.keys() for re in self.regexes)
        return set(itertools.chain(*keys))

    @cached_property
    def group_count(self):
        return sum(re.groups for re in self.regexes)


def _extract_urls(urlpatterns, parents):
    for pattern in urlpatterns:
        path = parents[:] + [pattern]
        if isinstance(pattern, url_resolver_types):
            yield from _extract_urls(pattern.url_patterns, path)
        else:
            yield URLEntry(path)


def extract_urls(urlpatterns=None):
    """Extract URLEntry objects from the given iterable of Django URL pattern objects.

    If no iterable is given, the patterns exposed by the root resolver are used, i.e.
    all of the URLs routed in the project.
    :param urlpatterns: Iterable of URLPattern objects
    :return: Generator of `URLEntry` objects.
    :rtype: list[URLEntry]
    """
    if urlpatterns is None:
        urlpatterns = urls.get_resolver(None).url_patterns
    yield from _extract_urls(urlpatterns, [])


def _get_all_form_errors(response):
    return (
        {
            key: [
                response.context[key].errors,
                getattr(response.context[key], 'non_form_errors', lambda: None)(),
            ]
            for key in response.context.keys()
            if key.endswith('_formset') or key == 'form' or key.endswith('_form')
        }
        if response.context
        else None
    )


class CheckFilePropertiesMixin:
    def _test_file_properties(self, file, **kwargs):
        if 'content_type' in kwargs:
            self.assertEqual(file.content_type, kwargs.get('content_type'))
        if 'get_status_display' in kwargs:
            self.assertEqual(file.get_status_display(), kwargs.get('get_status_display'))
        if 'get_type_display' in kwargs:
            self.assertEqual(file.get_type_display(), kwargs.get('get_type_display'))
        if 'hash' in kwargs:
            self.assertTrue(file.hash.startswith(kwargs.get('hash')), file.hash)
        if 'name' in kwargs:
            self.assertTrue(file.source.name.startswith(kwargs.get('name')), file.source.name)
        if 'original_hash' in kwargs:
            self.assertTrue(
                file.original_hash.startswith(kwargs.get('original_hash')), file.original_hash
            )
        if 'original_name' in kwargs:
            self.assertEqual(file.original_name, kwargs.get('original_name'))
        if 'size_bytes' in kwargs:
            self.assertEqual(file.size_bytes, kwargs.get('size_bytes'))


class _BaseCreateFile:
    submit_url = urls.reverse_lazy('extensions:submit')

    def setUp(self):
        super().setUp()

        self.user = UserFactory()
        self.temp_directory = tempfile.mkdtemp()

        file_data = {
            "name": "Blender Kitsu",
            "id": "blender_kitsu",
            "version": "0.1.5",
        }
        self.file = self._create_file_from_data("blender_kitsu_1.5.0.zip", file_data)

    def tearDown(self):
        super().tearDown()
        shutil.rmtree(self.temp_directory)

    def _create_file_from_data(self, filename, file_data, use_meta_data=True):
        output_path = os.path.join(self.temp_directory, filename)
        manifest_path = os.path.join(self.temp_directory, "blender_manifest.toml")

        if use_meta_data:
            combined_meta_data = META_DATA.copy()
            combined_meta_data.update(file_data)
        else:
            combined_meta_data = file_data

        version = combined_meta_data.get("version", "0.1.0")
        extension_id = combined_meta_data.get("id", "foobar").strip()
        type_slug = combined_meta_data.get('type')
        init_path = None

        if type_slug == 'add-on':
            # Add the required __init__.py file
            init_path = os.path.join(self.temp_directory, '__init__.py')
            with open(init_path, 'w') as init_file:
                init_file.write('')

        with open(manifest_path, "w") as manifest_file:
            toml.dump(combined_meta_data, manifest_file)

        with zipfile.ZipFile(output_path, "w") as my_zip:
            arcname = f"{extension_id}-{version}/{os.path.basename(manifest_path)}"
            my_zip.write(manifest_path, arcname=arcname)
            if init_path:
                # Write the __init__.py file too
                arcname = f"{extension_id}-{version}/{os.path.basename(init_path)}"
                my_zip.write(init_path, arcname=arcname)

        os.remove(manifest_path)
        return output_path


def create_user_token(*args, **kwargs) -> Tuple['UserToken', str]:
    token_key = UserToken.generate_token_key()
    kwargs['token_hash'] = UserToken.generate_hash(token_key)
    kwargs['token_prefix'] = UserToken.generate_token_prefix(token_key)
    token = UserToken.objects.create(*args, **kwargs)
    return token, token_key


def create_zipfile_object_from_path(path):
    if os.path.isdir(path):
        dir = path
        buf = io.BytesIO()
        with zipfile.ZipFile(buf, 'a', strict_timestamps=False) as zip:
            for path, _, files in os.walk(dir):
                for name in files:
                    filename = os.path.join(path, name)
                    arcname = os.path.relpath(filename, dir)
                    zip.write(filename, arcname=arcname)

        buf.seek(0)
        buf.name = os.path.basename(dir)
        return buf
    if os.path.isfile(path):
        return open(path, 'rb')

    raise RuntimeError(f'unknown path {path}')
