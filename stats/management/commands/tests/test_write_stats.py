from io import StringIO

from django.core.management import call_command
from django.test import TestCase

from common.tests.factories.extensions import create_approved_version
from stats.models import ExtensionDownload, ExtensionCountedStat


class WriteStatsCommandTest(TestCase):
    fixtures = ['dev', 'licenses']

    def test_command_updates_extensions_download_counters(self):
        out = StringIO()
        extension = create_approved_version().extension
        ExtensionDownload.objects.bulk_create(
            [
                ExtensionDownload(extension_id=extension.pk, ip_address='192.19.10.10'),
                ExtensionDownload(extension_id=extension.pk, ip_address='192.19.10.11'),
            ]
        )
        old_date_modified = extension.date_modified

        call_command('write_stats', stdout=out)

        extension.refresh_from_db()
        self.assertEqual(extension.download_count, 2)
        # auto-update field should not have changed on recount
        self.assertEqual(old_date_modified, extension.date_modified)
        # tables storing individual views still have them
        self.assertEqual(ExtensionDownload.objects.count(), 2)
        # last seen ID for this counter should have been stored
        self.assertEqual(
            ExtensionCountedStat.objects.get(field='download_count').last_seen_id,
            ExtensionDownload.objects.order_by('-id').first().pk,
        )

    def test_command_adds_extensions_download_counters(self):
        out = StringIO()
        extension = create_approved_version().extension
        extension.download_count = 10
        extension.save(update_fields={'download_count'})
        ExtensionDownload.objects.bulk_create(
            [
                ExtensionDownload(extension_id=extension.pk, ip_address='192.19.10.10'),
                ExtensionDownload(extension_id=extension.pk, ip_address='192.19.10.11'),
            ]
        )
        old_date_modified = extension.date_modified

        call_command('write_stats', stdout=out)

        extension.refresh_from_db()
        self.assertEqual(extension.download_count, 12)
        # auto-update field should not have changed on recount
        self.assertEqual(old_date_modified, extension.date_modified)
        # tables storing individual views still have them
        self.assertEqual(ExtensionDownload.objects.count(), 2)
        # last seen ID for this counter should have been stored
        self.assertEqual(
            ExtensionCountedStat.objects.get(field='download_count').last_seen_id,
            ExtensionDownload.objects.order_by('-id').first().pk,
        )
