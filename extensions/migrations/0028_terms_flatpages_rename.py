from django.db import migrations

def rename_flatpage(apps, schema_editor):
    FlatPage = apps.get_model('flatpages', 'FlatPage')
    try:
        # Rename 'policies' flatpage to 'terms-of-service'
        policies_flatpage = FlatPage.objects.get(url='/policies/')
        policies_flatpage.url = '/terms-of-service/'
        policies_flatpage.title = 'Terms of Service'
        policies_flatpage.save()
    except FlatPage.DoesNotExist:
        pass

    try:
        # Delete 'conditions-of-use' flatpage
        conditions_flatpage = FlatPage.objects.get(url='/conditions-of-use/')
        conditions_flatpage.delete()
    except FlatPage.DoesNotExist:
        pass

class Migration(migrations.Migration):

    dependencies = [
        ('flatpages', '__latest__'),
        ('extensions', '0029_extension_featured_image_extension_icon'),
    ]

    operations = [
        migrations.RunPython(rename_flatpage),
    ]
