from django.db import migrations
from django.core.management import call_command


def load_fixtures(apps, schema_editor):
    call_command('loaddata', 'extensions/fixtures/version_permissions.json')


class Migration(migrations.Migration):

    dependencies = [
        ('extensions', '0020_apply_about_page_and_permissions_fixtures'),
    ]

    operations = [
        migrations.RunPython(load_fixtures),
    ]
