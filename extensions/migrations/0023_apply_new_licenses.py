from django.db import migrations
from django.core.management import call_command


def load_fixtures(apps, schema_editor):
    call_command('loaddata', 'extensions/fixtures/licenses.json')


class Migration(migrations.Migration):

    dependencies = [
        ('extensions', '0022_alter_extension_type'),
    ]

    operations = [
        migrations.RunPython(load_fixtures),
    ]
