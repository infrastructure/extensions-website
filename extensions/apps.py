from django.apps import AppConfig


class ExtensionsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'extensions'

    def ready(self):
        from actstream import registry
        import extensions.signals  # noqa: F401

        registry.register(self.get_model('Extension'))
