from django.template import Library
from django.utils.translation import gettext_lazy as _

import extensions.models

register = Library()


@register.simple_tag(takes_context=True)
def has_maintainer(context, extension: extensions.models.Extension) -> bool:
    """Return True if current user is a maintainer of a given extension."""
    request = context.get('request')
    return extension.has_maintainer(request.user)


@register.simple_tag(takes_context=True)
def can_rate(context, extension: extensions.models.Extension) -> bool:
    """Return True if current user can rate a given extension."""
    request = context.get('request')
    return extension.can_rate(request.user)


@register.simple_tag(takes_context=True)
def get_hero_tabs(context, extension: extensions.models.Extension):
    request = context.get('request')
    extension_absolute_url = extension.get_absolute_url()
    extension_review_url = extension.get_review_url()
    latest_version = extension.latest_version
    hero_tabs = [
        {
            'condition': extension.is_listed,
            'href': extension_absolute_url,
            'is_active': request.path == extension_absolute_url,
            'label': _("About"),
        },
        {
            'condition': extension.is_listed and latest_version and latest_version.release_notes,
            'href': extension_absolute_url + '#new',
            'label': _("What's New"),
        },
        {
            'condition': (
                extension.is_listed and latest_version and latest_version.permissions_with_reasons
            ),
            'href': extension_absolute_url + '#permissions',
            'label': _("Permissions"),
        },
        {
            'condition': extension.is_listed,
            'href': extension.get_ratings_url(),
            'is_active': '/reviews/' in request.path,
            'label': _("Reviews"),
        },
        {
            'condition': not extension.is_listed,
            'href': extension_review_url + '#about',
            'label': _("About"),
        },
        {
            'condition': not extension.is_listed,
            'href': extension_review_url + '#activity',
            'label': _("Activity"),
        },
        {
            'condition': latest_version,
            'href': extension.get_versions_url(),
            'is_active': '/versions/' in request.path,
            'label': _("Version History"),
        },
    ]

    return filter(lambda t: t['condition'], hero_tabs)
