from django import template
from django.contrib.humanize.templatetags.humanize import naturaltime

register = template.Library()


@register.filter(name='naturaltime_compact')
def naturaltime_compact(time):
    """A more compact version of the humanize naturaltime filter."""
    compact_time = naturaltime(time)

    # Replace non-breaking space with regular space so it can be adjusted per case.
    compact_time = compact_time.replace(u'\xa0', u' ')

    # Take only the first part, e.g. "3 days, 2h ago", becomes " 3d ago"
    compact_time = compact_time.split(',')[0]

    compact_time = compact_time.replace(' ago', '')
    compact_time = compact_time.replace(' seconds', ' s')
    compact_time = compact_time.replace(' second', ' s')
    compact_time = compact_time.replace(' minutes', ' m')
    compact_time = compact_time.replace('a minute', '1 m')
    compact_time = compact_time.replace(' minute', ' m')
    compact_time = compact_time.replace(' hours', ' h')
    compact_time = compact_time.replace('an hour', '1 h')
    compact_time = compact_time.replace(' hour', ' h')
    compact_time = compact_time.replace(' days', ' d')
    compact_time = compact_time.replace(' day', ' d')
    compact_time = compact_time.replace(' weeks', ' w')
    compact_time = compact_time.replace(' week', ' w')
    compact_time = compact_time.replace(' months', ' mo')
    compact_time = compact_time.replace(' month', ' mo')
    compact_time = compact_time.replace(' years', ' y')
    compact_time = compact_time.replace(' year', ' y')

    return compact_time


@register.filter(name='int_compact')
def int_compact(number):
    """
    Round integer numbers
    123567  -> 123,6K
    1234567 -> 1,2M
    """
    number = str(number)

    if number.isdigit():
        number_int = int(number)

        if number_int >= 1000000:
            number = "%.0f%s" % (number_int / 1000000.00, 'M')
        else:
            if number_int >= 1000:
                number = "%.0f%s" % (number_int / 1000.0, 'K')
    return number
