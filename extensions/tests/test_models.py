import json

from django.core.exceptions import ValidationError
from django.test import TestCase

from common.admin import get_admin_change_path
from common.log_entries import entries_for
from common.tests.factories.extensions import create_version
from common.tests.factories.files import FileFactory
from common.tests.factories.users import UserFactory
from extensions.models import Platform
from users.tests.util import FakeMfaClient


class ExtensionTest(TestCase):
    client_class = FakeMfaClient
    fixtures = ['dev', 'licenses']
    maxDiff = None

    def setUp(self):
        super().setUp()
        self.extension = create_version(
            metadata__name='Extension name',
            metadata__website='https://example.com/',
        ).extension
        self.assertEqual(entries_for(self.extension).count(), 0)
        self.assertIsNone(self.extension.date_approved)
        self.assertIsNone(self.extension.date_status_changed)

    def _check_change_message(self):
        entries = entries_for(self.extension)
        self.assertEqual(entries.count(), 1)
        log_entry = entries.first()
        change_message = json.loads(log_entry.change_message)
        self.assertEqual(len(change_message), 1)
        self.assertDictEqual(
            change_message[0],
            {
                'changed': {
                    'fields': ['status'],
                    'name': 'extension',
                    'new_state': {'status': 'Approved'},
                    'object': '<Extension: Add-on "Extension name">',
                    'old_state': {
                        'description': '',
                        'name': 'Extension name',
                        'status': 1,
                        'support': None,
                        'website': 'https://example.com/',
                    },
                }
            },
        )

    def test_status_change_updates_date_creates_log_entry_with_update_fields(self):
        self.extension.approve()
        self.extension.refresh_from_db()

        self.assertIsNotNone(self.extension.date_approved)
        self.assertIsNotNone(self.extension.date_status_changed)
        self._check_change_message()

    def test_admin_change_view(self):
        path = get_admin_change_path(obj=self.extension)
        self.assertEqual(path, '/admin/extensions/extension/1/change/')

        admin_user = UserFactory(is_staff=True, is_superuser=True)
        self.client.force_login_with_mfa(admin_user)
        response = self.client.get(path)

        self.assertEqual(response.status_code, 200, path)


class VersionTest(TestCase):
    client_class = FakeMfaClient
    fixtures = ['dev', 'licenses']
    maxDiff = None

    def test_admin_change_view(self):
        version = create_version(
            metadata__blender_version_min='2.83.1',
            metadata__name='Extension name',
            metadata__version='1.1.2',
            metadata__website='https://example.com/',
        )
        self.assertEqual(entries_for(version).count(), 0)
        path = get_admin_change_path(obj=version)
        self.assertEqual(path, '/admin/extensions/version/1/change/')

        admin_user = UserFactory(is_staff=True, is_superuser=True)
        self.client.force_login_with_mfa(admin_user)
        response = self.client.get(path)

        self.assertEqual(response.status_code, 200, path)

    def test_get_remaining_platforms(self):
        version_platforms_none = create_version(metadata__platforms=None)
        self.assertEqual(version_platforms_none.get_remaining_platforms(), set())

        version_platforms_empty = create_version(metadata__platforms=[])
        self.assertEqual(version_platforms_empty.get_remaining_platforms(), set())

        version_platforms_some = create_version(metadata__platforms=['linux-x64', 'windows-x64'])
        self.assertTrue(version_platforms_some.get_remaining_platforms())

        version_two_files = create_version(metadata__platforms=['linux-x64'])
        file = FileFactory(metadata__platforms=['windows-x64'])
        version_two_files.files.add(file)
        self.assertTrue(version_two_files.get_remaining_platforms())

        version_platforms_all = create_version(
            metadata__platforms=[p.slug for p in Platform.objects.all()]
        )
        self.assertEqual(version_platforms_all.get_remaining_platforms(), set())

        skip_platforms = set(['linux-x64', 'windows-x64'])
        version_platforms_without_some = create_version(
            metadata__platforms=[
                p.slug for p in Platform.objects.all() if p.slug not in skip_platforms
            ]
        )
        self.assertEqual(version_platforms_without_some.get_remaining_platforms(), skip_platforms)

    def test_collect_platforms_across_files(self):
        version = create_version(metadata__platforms=['linux-x64'])
        file = FileFactory(metadata__platforms=['windows-x64'])
        version.add_file(file)
        self.assertQuerysetEqual(
            version.platforms.order_by('slug'),
            Platform.objects.filter(slug__in=['linux-x64', 'windows-x64']).order_by('slug'),
        )
        file.delete()
        version.refresh_from_db()
        self.assertQuerysetEqual(
            version.platforms.order_by('slug'),
            Platform.objects.filter(slug__in=['linux-x64']).order_by('slug'),
        )

    def test_add_file(self):
        version = create_version(metadata__platforms=['linux-x64'])
        file = FileFactory(metadata__platforms=['linux-x64'])
        with self.assertRaises(ValueError):
            version.add_file(file)

        all_platforms_version = create_version(metadata__platforms=[])
        file = FileFactory(metadata__platforms=['linux-x64'])
        with self.assertRaises(ValueError):
            all_platforms_version.add_file(file)

    def test_get_file_for_platform(self):
        version = create_version(metadata__platforms=['linux-x64'])
        file = FileFactory(metadata__platforms=['windows-x64'])
        version.add_file(file)
        self.assertIsNotNone(version.get_file_for_platform(None))
        self.assertIsNotNone(version.get_file_for_platform('linux-x64'))
        self.assertIsNotNone(version.get_file_for_platform('windows-x64'))
        self.assertIsNone(version.get_file_for_platform('windows-arm64'))

        file2 = FileFactory(metadata__platforms=['macos-x64', 'macos-arm64'])
        version.add_file(file2)
        self.assertIsNotNone(version.get_file_for_platform('macos-x64'))

        version2 = create_version(metadata__platforms=[])
        self.assertIsNotNone(version2.get_file_for_platform(None))
        self.assertIsNotNone(version2.get_file_for_platform('macos-x64'))

    def test_version_validation(self):
        with self.assertRaises(ValidationError):
            create_version(
                metadata__version='abc',
            )

        with self.assertRaises(ValidationError):
            create_version(
                metadata__version='1',
            )

        with self.assertRaises(ValidationError):
            create_version(
                metadata__version='1.2',
            )

        create_version(
            metadata__version='0.0.5+win-x64',
        )


class UpdateMetadataTest(TestCase):
    fixtures = ['dev', 'licenses']

    def setUp(self):
        super().setUp()
        self.first_version = create_version(
            metadata__name='name',
            metadata__website='https://example.com/',
        )
        self.extension = self.first_version.extension

    def test_version_create_and_delete(self):
        second_version = create_version(
            extension=self.extension,
            metadata__name='new name',
            metadata__website='https://example.com/new',
        )
        self.extension.refresh_from_db()
        self.assertEqual(self.extension.name, 'new name')
        self.assertEqual(self.extension.website, 'https://example.com/new')

        second_version.delete()
        self.extension.refresh_from_db()
        self.assertEqual(self.extension.name, 'name')
        self.assertEqual(self.extension.website, 'https://example.com/')

    def test_old_name_taken(self):
        second_version = create_version(
            extension=self.extension,
            metadata__name='new name',
            metadata__website='https://example.com/new',
        )

        # another extension uses old name
        create_version(
            metadata__name='name',
            metadata__website='https://example.com/',
        )

        second_version.delete()
        self.extension.refresh_from_db()
        # couldn't revert the name because it has been taken
        self.assertEqual(self.extension.name, 'new name')
        # reverted other fields
        self.assertEqual(self.extension.website, 'https://example.com/')
