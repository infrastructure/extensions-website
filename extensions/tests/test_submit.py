from pathlib import Path
from typing import List
import hashlib
import os

from django.test import TestCase
from django.urls import reverse_lazy

from common.tests.factories.extensions import create_version, create_approved_version
from common.tests.factories.files import FileFactory
from common.tests.factories.users import UserFactory
from common.tests.utils import (
    _get_all_form_errors, _BaseCreateFile, CheckFilePropertiesMixin, create_zipfile_object_from_path
)
from extensions.models import Extension, Version
from files.models import File
from reviewers.models import ApprovalActivity
from users.tests.util import FakeMfaClient
import utils


TEST_FILES_DIR = Path(__file__).resolve().parent / 'files'
EXPECTED_EXTENSION_DATA = {
    'addon-with-permissions.zip': {
        'metadata': {
            'tagline': 'Some add-on tag line',
            'name': 'Some Add-on',
            'id': 'some_addon',
            'version': '0.1.0',
            'blender_version_min': '4.2.0',
            'schema_version': '1.0.0',
            'type': 'add-on',
            'permissions': {'files': 'reading files', 'network': 'talking to server'},
            'platforms': ['linux-x64'],
        },
        'tags': [],
        'version_str': '0.1.0',
        'slug': 'some-addon',
    },
    'addon-with-split-platforms-linux.zip': {
        'metadata': {
            'tagline': 'Some add-on tag line',
            'name': 'Some Add-on',
            'id': 'some_addon',
            'version': '0.1.0',
            'blender_version_min': '4.2.0',
            'schema_version': '1.0.0',
            'type': 'add-on',
            'permissions': {'files': 'reading files', 'network': 'talking to server'},
            'platforms': ['linux-x64', 'windows-x64'],
            'build': {'generated': {'platforms': ['linux-x64']}},
        },
        'tags': [],
        'version_str': '0.1.0',
        'slug': 'some-addon',
    },
    'addon-without-dir.zip': {
        'metadata': {
            'tagline': 'Some add-on tag line',
            'name': 'Some Add-on',
            'id': 'some_addon',
            'version': '0.1.0',
            'blender_version_min': '4.2.0',
            'schema_version': '1.0.0',
            'type': 'add-on',
        },
        'tags': [],
        'version_str': '0.1.0',
        'slug': 'some-addon',
    },
}
EXPECTED_VALIDATION_ERRORS = {
    'empty.txt': {'source': ['Only .zip files are accepted.']},
    'empty.zip': {'source': ['The submitted file is empty.']},
    'invalid-archive.zip': {'source': ['Only .zip files are accepted.']},
    'invalid-manifest-path.zip': {
        'source': [
            'The manifest file should be at the top level of the archive, or one level deep.',
        ],
    },
    'invalid-addon-no-init.zip': {
        'source': ['Add-on file missing: <strong>__init__.py</strong>.'],
    },
    'invalid-addon-dir-no-init.zip': {
        'source': ['Add-on file missing: <strong>__init__.py</strong>.'],
    },
    'invalid-no-manifest.zip': {
        'source': ['The manifest file is missing.'],
    },
    'invalid-manifest-toml.zip': {
        'source': [
            'Manifest file contains invalid code: Found tokens after a closed string. Invalid TOML.'
            ' at line 8'
        ]
    },
    'invalid-theme-multiple-xmls.zip': {
        'source': ['Themes can only contain <strong>one XML file</strong>.']
    },
    'invalid-theme-multiple-xmls-macosx.zip': {
        'source': ['Archive contains forbidden files or directories: __MACOSX/']
    },
    'invalid-missing-wheels.zip': {
        'source': ['Python wheel missing: addon/wheels/test-wheel-whatever.whl']
    },
}
POST_DATA = {
    'preview_set-TOTAL_FORMS': ['0'],
    'preview_set-INITIAL_FORMS': ['0'],
    'preview_set-MIN_NUM_FORMS': ['0'],
    'preview_set-MAX_NUM_FORMS': ['1000'],
    'preview_set-0-id': [''],
    # 'preview_set-0-extension': [str(extension.pk)],
    'preview_set-1-id': [''],
    # 'preview_set-1-extension': [str(extension.pk)],
    'form-TOTAL_FORMS': ['0'],
    'form-INITIAL_FORMS': ['0'],
    'form-MIN_NUM_FORMS': ['0'],
    'form-MAX_NUM_FORMS': ['1000'],
    'form-0-id': '',
    # 'form-0-caption': ['First Preview Caption Text'],
    'form-1-id': '',
    # 'form-1-caption': ['Second Preview Caption Text'],
}


class SubmitFileTest(TestCase):
    client_class = FakeMfaClient
    fixtures = ['licenses']
    maxDiff = None
    url = reverse_lazy('extensions:submit')

    def setUp(self):
        super().setUp()
        self.user = UserFactory()
        self.client.force_login_with_mfa(self.user)

    def _test_submit_addon(
        self,
        file_name: str,
        name: str,
        tags: List[str],
        version_str: str,
        blender_version_min: str,
        slug: str,
        **other_metadata,
    ):
        self.assertEqual(File.objects.count(), 0)

        size_bytes = 0
        file_hash = ''
        h = hashlib.sha256()
        with create_zipfile_object_from_path(TEST_FILES_DIR / file_name) as fp:
            fp.seek(0, os.SEEK_END)
            size_bytes = fp.tell()
            fp.seek(0)
            h.update(fp.read(size_bytes))
            fp.seek(0)
            file_hash = 'sha256:' + h.hexdigest()
            response = self.client.post(self.url, {'source': fp, 'agreed_with_terms': True})

        self.assertEqual(response.status_code, 302, response.context)
        self.assertEqual(File.objects.count(), 1)
        file = File.objects.first()
        self.assertEqual(response['Location'], file.version.first().extension.get_draft_url())
        extension = file.version.first().extension
        self.assertEqual(extension.slug, slug)
        self.assertEqual(extension.name, name)
        self.assertEqual(file.original_name, file_name)
        self.assertEqual(file.size_bytes, size_bytes)
        self.assertEqual(file.original_hash, file_hash)
        self.assertEqual(file.hash, file_hash)
        self.assertEqual(file.get_type_display(), 'Add-on')
        self.assertEqual(file.metadata['version'], version_str)
        self.assertEqual(file.metadata.get('permissions'), other_metadata.get('permissions'))
        self.assertEqual(
            [p.slug for p in file.version.first().platforms.all()],
            other_metadata.get('build', {}).get('generated', {}).get('platforms', [])
            or other_metadata.get('platforms', []),
        )

    def test_not_allowed_anonymous(self):
        self.client.logout()
        with create_zipfile_object_from_path(TEST_FILES_DIR / 'addon-without-dir.zip') as fp:
            response = self.client.post(self.url, {'source': fp, 'agreed_with_terms': True})

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['Location'], f'/oauth/login?next={self.url}')

    def test_not_allowed_without_mfa(self):
        self.client.logout()
        # FIXME once the limitation is removed, remove this email override
        user = UserFactory(email='someone@blender.org')
        self.client.force_login(user)

        with create_zipfile_object_from_path(TEST_FILES_DIR / 'addon-without-dir.zip') as fp:
            response = self.client.post(self.url, {'source': fp, 'agreed_with_terms': True})

        self.assertEqual(response.status_code, 403)

    def test_validation_errors_agreed_with_terms_required(self):
        self.assertEqual(Extension.objects.count(), 0)

        with create_zipfile_object_from_path(TEST_FILES_DIR / 'theme.zip') as fp:
            response = self.client.post(self.url, {'source': fp})

        self.assertEqual(response.status_code, 200)
        self.assertDictEqual(
            response.context['form'].errors,
            {'agreed_with_terms': ['This field is required.']},
        )

    def test_validation_errors(self):
        self.assertEqual(Extension.objects.count(), 0)

        for test_archive, expected_errors in EXPECTED_VALIDATION_ERRORS.items():
            with self.subTest(test_archive=test_archive):
                with create_zipfile_object_from_path(TEST_FILES_DIR / test_archive) as fp:
                    response = self.client.post(self.url, {'source': fp, 'agreed_with_terms': True})

                self.assertEqual(response.status_code, 200)
                self.assertDictEqual(response.context['form'].errors, expected_errors)

    def test_addon_without_top_level_directory(self):
        self.assertEqual(Extension.objects.count(), 0)

        with create_zipfile_object_from_path(TEST_FILES_DIR / 'addon-without-dir.zip') as fp:
            response = self.client.post(self.url, {'source': fp, 'agreed_with_terms': True})

        self.assertEqual(response.status_code, 302)

    def test_theme_file(self):
        self.assertEqual(File.objects.count(), 0)

        with create_zipfile_object_from_path(TEST_FILES_DIR / 'theme.zip') as fp:
            response = self.client.post(self.url, {'source': fp, 'agreed_with_terms': True})

        self.assertEqual(response.status_code, 302, _get_all_form_errors(response))
        self.assertEqual(File.objects.count(), 1)
        file = File.objects.first()
        self.assertEqual(response['Location'], file.version.first().extension.get_draft_url())
        self.assertEqual(file.user, self.user)
        self.assertEqual(file.original_name, 'theme.zip')
        self.assertEqual(file.get_type_display(), 'Theme')
        self.assertEqual(file.metadata['name'], 'Theme')

    def test_keymap_file(self):
        self.assertEqual(File.objects.count(), 0)

        with create_zipfile_object_from_path(TEST_FILES_DIR / 'keymap.zip') as fp:
            response = self.client.post(self.url, {'source': fp, 'agreed_with_terms': True})

        self.assertEqual(response.status_code, 200, _get_all_form_errors(response))
        self.assertEqual(File.objects.count(), 0)

    def test_asset_bundle_file(self):
        self.assertEqual(File.objects.count(), 0)

        with create_zipfile_object_from_path(TEST_FILES_DIR / 'asset_bundle-0.1.0.zip') as fp:
            response = self.client.post(self.url, {'source': fp, 'agreed_with_terms': True})

        self.assertEqual(response.status_code, 200, _get_all_form_errors(response))
        self.assertEqual(File.objects.count(), 0)


for file_name, data in EXPECTED_EXTENSION_DATA.items():

    def _create_test_method(file_name, data):
        def _method(self):
            kwargs = data.copy()
            metadata = kwargs.pop('metadata')
            self._test_submit_addon(file_name, **kwargs, **metadata)

        return _method

    setattr(
        SubmitFileTest,
        f'test_submit_addon_file_{utils.slugify(file_name).replace("-", "_")}',
        _create_test_method(file_name, data),
    )


class SubmitFinaliseTest(CheckFilePropertiesMixin, TestCase):
    client_class = FakeMfaClient
    fixtures = ['licenses']
    maxDiff = None

    def setUp(self):
        super().setUp()
        file_data = EXPECTED_EXTENSION_DATA['addon-without-dir.zip']
        user = UserFactory()
        self.file = FileFactory(
            type=File.TYPES.BPY,
            user=user,
            metadata=file_data['metadata'],
        )
        self.version = create_version(file=self.file)

    def test_get_finalise_addon_redirects_if_anonymous(self):
        response = self.client.post(self.file.version.first().extension.get_draft_url(), {})

        self.assertEqual(response.status_code, 302)
        self.assertEqual(
            response['Location'],
            f'/oauth/login?next=/add-ons/{self.file.version.first().extension.slug}/draft/',
        )

    def test_get_finalise_addon_not_allowed_if_different_user(self):
        user = UserFactory()
        self.client.force_login_with_mfa(user)

        response = self.client.post(self.file.version.first().extension.get_draft_url(), {})

        # Technically this could (should) be a 403, but changing this means changing
        # the MaintainedExtensionMixin which is used in multiple places.
        self.assertEqual(response.status_code, 404)

    def test_post_finalise_addon_validation_errors(self):
        self.client.force_login_with_mfa(self.file.user)
        data = {**POST_DATA, 'submit_draft': ''}
        response = self.client.post(self.file.version.first().extension.get_draft_url(), data)

        self.assertEqual(response.status_code, 200)
        self.assertDictEqual(
            _get_all_form_errors(response),
            {
                'form': [{}, None],
                'extension_form': [{'description': ['This field is required.']}, None],
                'add_preview_formset': [[], ['Please add at least one preview.']],
                'edit_preview_formset': [[], []],
                'featured_image_form': [{'source': ['This field is required.']}, None],
                'icon_form': [{'source': ['This field is required.']}, None],
                'image_form': [{'source': ['This field is required.']}, None],
            },
        )

    def test_post_finalise_addon_creates_addon_with_version_awaiting_review(self):
        self.assertEqual(File.objects.count(), 1)
        self.assertEqual(Extension.objects.count(), 1)
        self.assertEqual(Version.objects.count(), 1)
        images_count_before = File.objects.filter(type=File.TYPES.IMAGE).count()

        self.client.force_login_with_mfa(self.file.user)
        data = {
            # Most of these values should come from the form's initial values, set in the template
            # Version fields
            'release_notes': 'initial release',
            'blender_version_max': '4.5.0',
            # Extension fields
            'description': 'Rather long and verbose description',
            'support': 'https://example.com/issues',
            # Previews
            'form-TOTAL_FORMS': ['2'],
            'form-INITIAL_FORMS': ['0'],
            'form-MIN_NUM_FORMS': ['0'],
            'form-MAX_NUM_FORMS': ['1000'],
            'form-0-id': '',
            'form-0-caption': ['First Preview Caption Text'],
            'form-1-id': '',
            'form-1-caption': ['Second Preview Caption Text'],
            # Edit form for existing previews
            'preview_set-TOTAL_FORMS': ['0'],
            'preview_set-INITIAL_FORMS': ['0'],
            'preview_set-MIN_NUM_FORMS': ['0'],
            'preview_set-MAX_NUM_FORMS': ['1000'],
            'preview_set-0-id': [''],
            # 'preview_set-0-extension': [str(extension.pk)],
            'preview_set-1-id': [''],
            # 'preview_set-1-extension': [str(extension.pk)],
            # Submit for Approval.
            'submit_draft': '',
        }
        file_name1 = 'test_preview_image_0001.png'
        file_name2 = 'test_preview_image_0002.png'
        file_name3 = 'test_icon_0001.png'
        file_name4 = 'test_featured_image_0001.png'
        with open(TEST_FILES_DIR / file_name1, 'rb') as fp1, open(
            TEST_FILES_DIR / file_name2, 'rb'
        ) as fp2, open(TEST_FILES_DIR / file_name3, 'rb') as fp3, open(
            TEST_FILES_DIR / file_name4, 'rb'
        ) as fp4:
            files = {
                'form-0-source': fp1,
                'form-1-source': fp2,
                'icon-source': fp3,
                'featured-image-source': fp4,
            }
            response = self.client.post(
                self.file.version.first().extension.get_draft_url(), {**data, **files}
            )

        self.assertEqual(response.status_code, 302, _get_all_form_errors(response))
        self.assertEqual(response['Location'], '/add-ons/some-addon/manage/')
        self.assertEqual(File.objects.filter(type=File.TYPES.BPY).count(), 1)
        self.assertEqual(Extension.objects.count(), 1)
        self.assertEqual(Version.objects.count(), 1)
        # Check that 4 new images had been created
        self.assertEqual(
            File.objects.filter(type=File.TYPES.IMAGE).count(), images_count_before + 4
        )
        # Check an add-on was created with all given fields
        extension = Extension.objects.first()
        self.assertEqual(extension.get_type_display(), 'Add-on')
        self.assertEqual(extension.get_status_display(), 'Awaiting Review')
        self.assertEqual(extension.name, 'Some Add-on')
        self.assertEqual(extension.website, None)
        self.assertEqual(extension.support, data['support'])
        self.assertEqual(extension.description, data['description'])
        self.assertEqual(list(extension.authors.all()), [self.file.user])
        # Check an add-on version was created with all given fields
        version = extension.latest_version
        self.assertEqual(version.version, '0.1.0')
        self.assertEqual(version.blender_version_min, '4.2.0')
        self.assertEqual(version.blender_version_max, '4.5.0')
        self.assertEqual(version.schema_version, '1.0.0')
        self.assertEqual(version.release_notes, data['release_notes'])
        # Check version file properties
        self._test_file_properties(
            version.files.first(),
            content_type='application/zip',
            get_status_display='Awaiting Review',
            get_type_display='Add-on',
            hash=version.files.first().original_hash,
            original_hash=version.files.first().original_hash,
        )
        # We cannot check for the ManyToMany yet (tags, licences, permissions)

        # Check icon file properties
        self._test_file_properties(
            extension.icon,
            content_type='image/png',
            get_status_display='Awaiting Review',
            get_type_display='Image',
            hash=extension.icon.original_hash,
            name='images/ee/ee3a015',
            original_hash='sha256:ee3a015',
            original_name='test_icon_0001.png',
            size_bytes=30177,
        )

        # Check featured image file properties
        self._test_file_properties(
            extension.featured_image,
            content_type='image/png',
            get_status_display='Awaiting Review',
            get_type_display='Image',
            hash=extension.featured_image.original_hash,
            name='images/a3/a3f445bfadc6a',
            original_hash='sha256:a3f445bfadc6a',
            original_name='test_featured_image_0001.png',
            size_bytes=155684,
        )

        # Check properties of preview image files
        self._test_file_properties(
            extension.previews.all()[0],
            content_type='image/png',
            get_status_display='Awaiting Review',
            get_type_display='Image',
            hash='sha256:643e15',
            name='images/64/643e15',
            original_hash='sha256:643e15',
            original_name='test_preview_image_0001.png',
            size_bytes=1163,
        )
        self._test_file_properties(
            extension.previews.all()[1],
            content_type='image/png',
            get_status_display='Awaiting Review',
            get_type_display='Image',
            hash='sha256:f8ef448d',
            name='images/f8/f8ef448d',
            original_hash='sha256:f8ef448d',
            original_name='test_preview_image_0002.png',
            size_bytes=1693,
        )

        # Check that author can access the page they are redirected to
        response = self.client.get(response['Location'])
        self.assertEqual(response.status_code, 200)


class NewVersionTest(TestCase):
    client_class = FakeMfaClient
    fixtures = ['licenses']

    def setUp(self):
        self.version = create_version(metadata__id='some_addon')
        self.extension = self.version.extension
        self.url = self.extension.get_new_version_url()
        self.client.force_login_with_mfa(self.version.files.first().user)

    def test_not_allowed_anonymous(self):
        self.client.logout()
        with create_zipfile_object_from_path(TEST_FILES_DIR / 'addon-without-dir.zip') as fp:
            response = self.client.post(self.url, {'source': fp, 'agreed_with_terms': True})

        self.assertEqual(response.status_code, 302)
        self.assertTrue(response['Location'].startswith('/oauth/login'))

    def test_validation_errors_agreed_with_terms_required(self):
        with create_zipfile_object_from_path(TEST_FILES_DIR / 'addon-without-dir.zip') as fp:
            response = self.client.post(self.url, {'source': fp})

        self.assertEqual(response.status_code, 200)
        self.assertDictEqual(
            response.context['form'].errors,
            {'agreed_with_terms': ['This field is required.']},
        )

    def test_validation_errors_invalid_extension(self):
        with create_zipfile_object_from_path(TEST_FILES_DIR / 'empty.txt') as fp:
            response = self.client.post(self.url, {'source': fp, 'agreed_with_terms': True})

        self.assertEqual(response.status_code, 200)
        self.assertDictEqual(
            response.context['form'].errors,
            {'source': ['Only .zip files are accepted.']},
        )

    def test_validation_errors_empty_file(self):
        with create_zipfile_object_from_path(TEST_FILES_DIR / 'empty.zip') as fp:
            response = self.client.post(self.url, {'source': fp, 'agreed_with_terms': True})

        self.assertEqual(response.status_code, 200)
        self.assertDictEqual(
            response.context['form'].errors,
            {'source': ['The submitted file is empty.']},
        )

    def test_upload_new_file_and_finalise_new_version(self):
        self.extension.approve()

        # Check step 1: upload a new file
        with create_zipfile_object_from_path(TEST_FILES_DIR / 'addon-without-dir.zip') as fp:
            response = self.client.post(self.url, {'source': fp, 'agreed_with_terms': True})

        self.assertEqual(response.status_code, 302)
        file = File.objects.order_by('-date_created').first()
        self.assertEqual(
            response['Location'],
            f'/add-ons/{self.extension.slug}/manage/versions/new/{file.pk}/',
        )
        # now a file upload creates a corresponding version object immediately
        self.assertEqual(self.extension.versions.count(), 2)
        new_version = self.extension.versions.order_by('date_created').last()
        self.assertEqual(new_version.version, '0.1.0')
        self.assertEqual(new_version.blender_version_min, '4.2.0')
        self.assertEqual(new_version.schema_version, '1.0.0')
        self.assertEqual(new_version.files.first().get_status_display(), 'Approved')
        self.assertEqual(new_version.release_notes, '')
        self.assertEqual(
            ApprovalActivity.objects.filter(
                extension=self.extension,
                type=ApprovalActivity.ActivityType.UPLOADED_NEW_VERSION,
            ).count(),
            1,
        )

        url = response['Location']
        response = self.client.post(
            url,
            {
                'release_notes': 'new version',
            },
        )

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['Location'], f'/add-ons/{self.extension.slug}/manage/versions/')
        self.assertEqual(self.extension.versions.count(), 2)
        new_version.refresh_from_db()
        self.assertEqual(new_version.release_notes, 'new version')


class MultiPlatformUploadTest(TestCase):
    client_class = FakeMfaClient

    def test_upload_more_files(self):
        extension = create_version(
            metadata__id="some_addon",
            metadata__version="0.0.1",
        ).extension
        file_linux = TEST_FILES_DIR / 'addon-with-split-platforms-linux.zip'
        file_windows = TEST_FILES_DIR / 'addon-with-split-platforms-windows.zip'
        file_no_platforms = TEST_FILES_DIR / 'addon-without-platforms-for-split-test.zip'
        self.client.force_login_with_mfa(extension.authors.all()[0])

        with create_zipfile_object_from_path(file_linux) as fp:
            response = self.client.post(
                extension.get_new_version_url(),
                {'source': fp, 'agreed_with_terms': True},
            )
        self.assertEqual(response.status_code, 302)
        extension.refresh_from_db()
        self.assertEqual(extension.latest_version.files.count(), 1)
        self.assertEqual(extension.latest_version.platforms.count(), 1)

        with create_zipfile_object_from_path(file_windows) as fp:
            response = self.client.post(
                extension.get_new_version_url(),
                {'source': fp, 'agreed_with_terms': True},
            )
        self.assertEqual(response.status_code, 302)
        extension.refresh_from_db()
        self.assertEqual(extension.latest_version.files.count(), 2)
        self.assertEqual(extension.latest_version.platforms.count(), 2)

        # can't upload the same file a second time
        with create_zipfile_object_from_path(file_windows) as fp:
            response = self.client.post(
                extension.get_new_version_url(),
                {'source': fp, 'agreed_with_terms': True},
            )
        self.assertEqual(response.status_code, 200)
        extension.refresh_from_db()
        self.assertEqual(extension.latest_version.files.count(), 2)
        self.assertEqual(extension.latest_version.platforms.count(), 2)

        # can't upload a platform-agnostic file
        with create_zipfile_object_from_path(file_no_platforms) as fp:
            response = self.client.post(
                extension.get_new_version_url(),
                {'source': fp, 'agreed_with_terms': True},
            )
        self.assertEqual(response.status_code, 200)
        extension.refresh_from_db()
        self.assertEqual(extension.latest_version.files.count(), 2)
        self.assertEqual(extension.latest_version.platforms.count(), 2)


class DraftsWarningTest(TestCase):
    client_class = FakeMfaClient
    fixtures = ['licenses']

    def test_page_contains_warning(self):
        version = create_version()
        extension = version.extension
        self.assertEqual(extension.status, Extension.STATUSES.DRAFT)
        self.client.force_login_with_mfa(extension.authors.all()[0])
        response = self.client.get(reverse_lazy('extensions:submit'))
        self.assertContains(response, extension.get_draft_url())


class VersionMaxTest(_BaseCreateFile, TestCase):
    client_class = FakeMfaClient

    def setUp(self):
        super().setUp()

        self.version_original = create_approved_version(
            metadata__id='blender_kitsu',
            metadata__name='Blender Kitsu',
            metadata__version='0.1.5-alpha+f52258de',
        )
        self.extension = self.version_original.extension
        self.client.force_login_with_mfa(self.version_original.files.first().user)

    def _make_file(self, file_data):
        # Submit the file.
        extension_file = self._create_file_from_data("kitsu-0.1.6.zip", file_data)
        with open(extension_file, 'rb') as fp:
            self.response = self.client.post(
                self.extension.get_new_version_url(), {'source': fp, 'agreed_with_terms': True}
            )
        self.assertEqual(self.response.status_code, 302)

        # The version needs to be approved.
        _file = File.objects.filter(original_name='kitsu-0.1.6.zip').first()
        _file.status = File.STATUSES.APPROVED
        _file.save()

        self.extension.refresh_from_db()
        # Make sure the version was updated.
        self.assertNotEqual(self.version_original, self.extension.latest_version)
        self.assertEqual(self.extension.latest_version.version, file_data['version'])

    def _finalize_version(self, data, status_code=302):
        url = self.response['Location']
        form_data = data.copy()
        # Add release_notes for control - to be sure something was passed.
        form_data['release_notes'] = "anything"

        response = self.client.post(url, form_data)
        self.assertEqual(response.status_code, status_code)

        if status_code != 302:
            return

        self.extension.refresh_from_db()
        self.assertEqual(self.extension.latest_version.release_notes, "anything")

    def test_max_set_not_in_manifest__not_set_on_form(self):
        """Test a scenario where the blender_version_max is not present in
        the manifest, and is not set by users when finalizing the upload.
        """
        kitsu_0_1_6 = {
            "id": "blender_kitsu",
            "version": "0.1.6",
            "blender_version_max": None,
        }

        self._make_file(kitsu_0_1_6)
        self.assertIsNone(self.extension.latest_version.blender_version_max)

        finalize_data = {}

        self._finalize_version(finalize_data)
        self.assertIsNone(self.extension.latest_version.blender_version_max)

    def test_max_set_not_in_manifest__set_on_form(self):
        """Test a scenario where the blender_version_max is not present in
        the manifest, but is set by users when finalizing the upload.
        """
        kitsu_0_1_6 = {
            "id": "blender_kitsu",
            "version": "0.1.6",
            "blender_version_max": None,
        }

        self._make_file(kitsu_0_1_6)
        self.assertIsNone(self.extension.latest_version.blender_version_max)

        finalize_data = {
            'blender_version_max': '5.3.0',
        }

        self._finalize_version(finalize_data)
        self.assertEqual(self.extension.latest_version.blender_version_max, '5.3.0')

    def test_max_set_in_manifest__set_on_form(self):
        """Test a scenario where the blender_version_max is present in
        the manifest, and it was also set by users when finalizing the upload.
        """
        kitsu_0_1_6 = {
            "id": "blender_kitsu",
            "version": "0.1.6",
            "blender_version_max": "4.3.0",
        }

        self._make_file(kitsu_0_1_6)
        self.assertEqual(self.extension.latest_version.blender_version_max, "4.3.0")

        finalize_data = {
            'blender_version_max': '4.4.0',
        }

        self._finalize_version(finalize_data)
        self.assertEqual(self.extension.latest_version.blender_version_max, '4.4.0')

    def test_max_wrongly_set(self):
        """Set a max version which is lower than min version"""
        kitsu_0_1_6 = {
            "id": "blender_kitsu",
            "version": "0.1.6",
            "blender_version_min": "4.4.0",
            "blender_version_max": "4.6.0",
        }

        self._make_file(kitsu_0_1_6)
        latest_version = self.extension.latest_version
        self.assertEqual(latest_version.blender_version_min, "4.4.0")
        self.assertEqual(latest_version.blender_version_max, "4.6.0")

        finalize_data = {
            'blender_version_max': '4.3.0',
        }

        self._finalize_version(finalize_data, status_code=200)
