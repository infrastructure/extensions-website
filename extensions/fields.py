from django.core.exceptions import ValidationError
from django.db.models import CharField
from semantic_version import Version


def validate_version_string(version_string):
    try:
        Version.parse(version_string)
    except ValueError as e:
        raise ValidationError(e)


class VersionStringField(CharField):
    description = "A field to store serializable semantic versions"

    default_validators = [validate_version_string]
