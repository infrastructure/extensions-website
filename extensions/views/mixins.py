from django.contrib.auth.mixins import UserPassesTestMixin
from django.shortcuts import get_object_or_404

from extensions.models import Extension
from files.models import File


class OwnsFileMixin(UserPassesTestMixin):
    def dispatch(self, *args, **kwargs):
        self.file = get_object_or_404(File, pk=self.kwargs['pk'])
        return super().dispatch(*args, **kwargs)

    def test_func(self) -> bool:
        return self.file.user == self.request.user


class MaintainedExtensionMixin:
    """Fetch an extension by slug if current user is a maintainer."""

    def dispatch(self, *args, **kwargs):
        self.extension = get_object_or_404(
            Extension.objects.authored_by(self.request.user).prefetch_related(
                'versions',
                'versions__files',
            ),
            slug=self.kwargs['slug'],
        )
        return super().dispatch(*args, **kwargs)

    def get_object(self):
        return self.extension


class ListedExtensionMixin:
    """Fetch a publicly listed extension by slug in the URL before dispatching the view."""

    def dispatch(self, *args, **kwargs):
        self.extension = get_object_or_404(
            Extension.objects.listed.prefetch_related('authors', 'ratings'),
            slug=self.kwargs['slug'],
        )
        return super().dispatch(*args, **kwargs)


class ExtensionMixin:
    """Fetch an extension by slug in the URL before dispatching the view."""

    def dispatch(self, *args, **kwargs):
        self.extension = get_object_or_404(Extension, slug=self.kwargs['slug'])
        return super().dispatch(*args, **kwargs)


class DraftVersionMixin:
    """Fetch the version object which is being edited as a draft."""

    def dispatch(self, *args, **kwargs):
        self.version = self.extension.versions.first()
        return super().dispatch(*args, **kwargs)
