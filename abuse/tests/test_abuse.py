from django.test import TestCase

from abuse.models import AbuseReport
from common.tests.factories.abuse import AbuseReportFactory
from common.tests.factories.extensions import RatingFactory, create_approved_version
from common.tests.factories.users import UserFactory, create_moderator
from constants.base import ABUSE_TYPE_RATING
from notifications.models import Notification
from ratings.models import Rating

POST_DATA = {
    'message': 'test message',
    'reason': '127',
    'version': '',
}


class ReportTest(TestCase):
    def test_report_twice(self):
        version = create_approved_version()
        user = UserFactory()
        self.client.force_login(user)
        url = version.extension.get_report_url()
        _ = self.client.post(url, POST_DATA)
        response = self.client.get(url, follow=True)
        self.assertEqual(response.status_code, 200)

    def test_only_reporter_and_moderator_can_view_report_abt_user(self):
        reported_user = UserFactory()
        report = AbuseReportFactory(user=reported_user)
        self.assertIsNone(report.extension)
        report_url = report.get_absolute_url()

        # Check that anonymous gets redirected (to login)
        self.assertEqual(self.client.get(report_url).status_code, 302)

        # Check that reported user cannot view the report
        self.client.force_login(reported_user)
        self.assertEqual(self.client.get(report_url).status_code, 404)

        # Check that reporter and moderator can view the report
        for account, role in (
            (report.reporter, 'reporter'),
            (create_moderator(), 'moderator'),
        ):
            with self.subTest(role=role):
                self.client.logout()
                self.client.force_login(account)
                self.assertEqual(self.client.get(report_url).status_code, 200)

    def test_only_reporter_and_moderator_can_view_report_abt_extension(self):
        report = AbuseReportFactory(user=None, extension=create_approved_version().extension)
        self.assertIsNone(report.user)
        report_url = report.get_absolute_url()

        # Check that anonymous gets redirected (to login)
        self.assertEqual(self.client.get(report_url).status_code, 302)

        # Check that reporter and moderator can view the report
        for account, role in (
            (report.reporter, 'reporter'),
            (create_moderator(), 'moderator'),
        ):
            with self.subTest(role=role):
                self.client.logout()
                self.client.force_login(account)
                self.assertEqual(self.client.get(report_url).status_code, 200)


class ResolveReportTest(TestCase):
    def test_reporter_gets_notified(self):
        report = AbuseReportFactory(
            extension=create_approved_version().extension,
            status=AbuseReport.STATUSES.UNTRIAGED,
        )
        notification_nr = Notification.objects.filter(recipient=report.reporter).count()
        moderator = create_moderator()
        self.client.force_login(moderator)
        response = self.client.post(
            report.get_absolute_url(), {'moderator_note': 'lalala', 'resolve': ''}
        )
        self.assertEqual(response.status_code, 302)
        report.refresh_from_db()
        self.assertEqual(report.status, AbuseReport.STATUSES.RESOLVED)
        self.assertEqual(report.processed_by, moderator)
        new_notification_nr = Notification.objects.filter(recipient=report.reporter).count()
        self.assertEqual(new_notification_nr, notification_nr + 1)

    def test_rating_is_rejected(self):
        version = create_approved_version()
        extension = version.extension
        some_user = UserFactory()
        rating = RatingFactory(user=some_user, version=version, status=Rating.STATUSES.APPROVED)
        report = AbuseReportFactory(
            extension=extension,
            status=AbuseReport.STATUSES.UNTRIAGED,
            rating=rating,
            type=ABUSE_TYPE_RATING,
        )
        moderator = create_moderator()
        self.client.force_login(moderator)
        response = self.client.post(
            report.get_absolute_url(), {'moderator_note': 'lalala', 'resolve': ''}
        )
        self.assertEqual(response.status_code, 302)
        rating.refresh_from_db()
        self.assertEqual(rating.status, Rating.STATUSES.REJECTED)
