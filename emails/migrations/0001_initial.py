# Generated by Django 4.0.6 on 2024-02-23 23:12

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Email',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_created', models.DateTimeField(auto_now_add=True)),
                ('date_modified', models.DateTimeField(auto_now=True)),
                ('subject', models.CharField(max_length=255)),
                ('from_email', models.CharField(max_length=255)),
                ('reply_to', models.CharField(help_text='A single valid email address or a comma-separated list of valid email addresses', max_length=255)),
                ('to', models.CharField(help_text='A single valid email address or a comma-separated list of valid email addresses', max_length=255)),
                ('message', models.TextField(help_text='Plain text body of the email')),
                ('html_message', models.TextField(help_text='HTML version of the email')),
                ('base_html_template', models.CharField(blank=True, default='emails/email.html', help_text='Base template to use when rendering the HTML version of the email', max_length=255)),
                ('date_sent', models.DateTimeField(blank=True, null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='EmailPreview',
            fields=[
            ],
            options={
                'managed': False,
                'proxy': True,
            },
            bases=('emails.email',),
        ),
    ]
