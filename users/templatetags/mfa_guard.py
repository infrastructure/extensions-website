from django import template
from django.urls import reverse

register = template.Library()


class MFAGuardNode(template.Node):
    def __init__(self, action_name, nodelist):
        self.action_name = action_name
        self.nodelist = nodelist

    def render(self, context):
        if context['user_has_mfa']:
            return self.nodelist.render(context)
        else:
            t = context.template.engine.get_template('mfa_guard.html')
            return t.render(
                template.Context(
                    {
                        'action_name': self.action_name,
                        'mfa_url': reverse('users:logout-before-mfa-setup'),
                    },
                    autoescape=context.autoescape,
                )
            )


@register.tag(name="mfa_guard")
def do_mfa_guard(parser, token):
    """Usage: {% mfa_guard 'your action name' %} ... {% endmfa_guard %}

    If current user has MFA configured the template between the tags will be rendered as is,
    otherwise a helpful message explaining MFA requirement will be displayed instead.
    """
    try:
        tag_name, action_name = token.split_contents()
        nodelist = parser.parse(("endmfa_guard",))
        parser.delete_first_token()
        return MFAGuardNode(action_name[1:-1], nodelist)
    except ValueError:
        raise template.TemplateSyntaxError(
            "%r tag requires exactly one argument: action_name" % token.contents.split()[0]
        )
