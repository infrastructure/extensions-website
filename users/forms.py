from django import forms

from constants.activity import Verb


class NotificationPreferencesForm(forms.Form):
    choices = [
        (Verb.COMMENTED, 'Comments'),
        (Verb.UPDATED_EXTENSION, 'Description & Image Changes'),
        (Verb.UPLOADED_NEW_VERSION, 'Version Uploads'),
    ]
    supported_verbs = [c[0] for c in choices]

    notification_preferences = forms.MultipleChoiceField(
        choices=choices,
        label='You can disable notifications for some activity types',
        required=False,
        widget=forms.CheckboxSelectMultiple,
    )


class SubscribeNotificationEmailsForm(forms.Form):
    subscribe = forms.BooleanField(widget=forms.HiddenInput(), required=False)
