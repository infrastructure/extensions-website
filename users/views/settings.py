"""User profile pages."""
from django.contrib.auth import get_user_model
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db import transaction
from django.urls import reverse
from django.views.generic import TemplateView
from django.views.generic.edit import FormView

from notifications.models import NotificationPreference
from users.forms import NotificationPreferencesForm, SubscribeNotificationEmailsForm

User = get_user_model()


class ProfileView(LoginRequiredMixin, TemplateView):
    """Template view for the profile settings."""

    template_name = 'users/settings/profile.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        deactivated_notifications = list(
            self.request.user.notificationpreference_set.filter(is_deactivated=True).values_list(
                'verb', flat=True
            )
        )
        context['notification_preferences_form'] = NotificationPreferencesForm(
            {
                'notification_preferences': [
                    v for v in NotificationPreferencesForm.supported_verbs
                    if v not in deactivated_notifications
                ],
            },
        )
        context['subscribe_notification_emails_form'] = SubscribeNotificationEmailsForm(
            {'subscribe': not self.request.user.is_subscribed_to_notification_emails},
        )
        return context


class DeleteView(LoginRequiredMixin, TemplateView):
    """Template view where account deletion can be requested."""

    template_name = 'users/settings/delete.html'


class SaveNotificationPreferencesView(LoginRequiredMixin, FormView):
    form_class = NotificationPreferencesForm

    def get_success_url(self):
        return reverse('users:my-profile') + '#notifications'

    @transaction.atomic
    def form_valid(self, form):
        deactivated_notifications = set(
            self.request.user.notificationpreference_set.filter(is_deactivated=True).values_list(
                'verb', flat=True
            )
        )
        to_deactivate = (
            set(NotificationPreferencesForm.supported_verbs)
            - set(form.cleaned_data['notification_preferences'])
        )
        to_delete = deactivated_notifications - to_deactivate
        to_create = to_deactivate - deactivated_notifications
        self.request.user.notificationpreference_set.filter(verb__in=to_delete).delete()
        self.request.user.notificationpreference_set.set(
            [
                NotificationPreference(is_deactivated=True, user=self.request.user, verb=v)
                for v in to_create
            ],
            bulk=False,
        )
        return super().form_valid(form)


class SubscribeNotificationEmailsView(LoginRequiredMixin, FormView):
    form_class = SubscribeNotificationEmailsForm

    def get_success_url(self):
        return reverse('users:my-profile') + '#notifications'

    def form_valid(self, form):
        self.request.user.is_subscribed_to_notification_emails = form.cleaned_data['subscribe']
        self.request.user.save(update_fields={'is_subscribed_to_notification_emails'})
        return super().form_valid(form)
