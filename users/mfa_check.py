from django.conf import settings
from django.contrib.auth import logout
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect


def user_has_mfa(request):
    if not request.user.is_authenticated:
        return False

    return (
        'id_token' in request.session
        and 'amr' in request.session['id_token']
        and 'mfa' in request.session['id_token']['amr']
    )


def logout_mfa_redirect(request):
    logout(request)
    return HttpResponseRedirect(settings.BLENDER_ID['BASE_URL'] + 'mfa/')


class MfaRequiredMixin(LoginRequiredMixin):
    """Verify that the current user is authenticated using Multi-factor Authentication."""

    permission_denied_message = 'MFA required'

    def dispatch(self, request, *args, **kwargs):
        if not user_has_mfa(request):
            return self.handle_no_permission()
        return super().dispatch(request, *args, **kwargs)
