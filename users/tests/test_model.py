from datetime import datetime, timezone

from django.test import TestCase

from common.tests.factories.extensions import create_version
from common.tests.factories.reviewers import ApprovalActivityFactory
from common.tests.factories.users import create_moderator, UserFactory


class TestModel(TestCase):
    def test_has_special_role(self):
        moderator = create_moderator()
        staff = UserFactory(is_staff=True)
        superuser = UserFactory(is_superuser=True, is_staff=True)
        for account, role in (
            (moderator, 'moderator'),
            (staff, 'staff'),
            (superuser, 'superuser'),
        ):
            with self.subTest(role=role):
                self.assertTrue(account.has_special_role)

    def test_has_protected_data(self):
        reviewer = UserFactory(is_staff=False)
        ApprovalActivityFactory(user=reviewer, extension=create_version().extension)
        self.assertTrue(reviewer.has_protected_data)

        author = UserFactory()
        create_version(user=author)
        self.assertTrue(author.has_protected_data)

        nobody = UserFactory()
        self.assertFalse(nobody.has_protected_data)

    def test_request_deletion_does_nothing_when_cannot_be_deleted(self):
        moderator = create_moderator()
        staff = UserFactory(is_staff=True)
        superuser = UserFactory(is_superuser=True, is_staff=True)
        for account, role in (
            (moderator, 'moderator'),
            (staff, 'staff'),
            (superuser, 'superuser'),
        ):
            with self.subTest(role=role), self.assertLogs('users.models', level='WARNING') as log:
                account.request_deletion('')
                self.assertIn(
                    f'User pk={account.pk} has special role, ignoring deletion request',
                    log.output[0],
                )
                account.refresh_from_db()
                self.assertIsNone(account.date_deletion_requested)
                self.assertTrue(account.is_active)

    def test_request_deletion_deactivates_account(self):
        account = UserFactory()

        with self.assertLogs('users.models', level='WARNING') as log:
            account.request_deletion('2020-12-31T23:02:03+00:00')
            self.assertIn(
                f'Deletion of User pk={account.pk} requested on 2020-12-31T23:02:03+00:00, deactivating this account',
                log.output[0],
            )

        account.refresh_from_db()
        self.assertEqual(
            account.date_deletion_requested, datetime(2020, 12, 31, 23, 2, 3, tzinfo=timezone.utc)
        )
        self.assertFalse(account.is_active)
