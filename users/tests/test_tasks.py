from datetime import timedelta

from django.contrib.auth import get_user_model
from django.test import TestCase
from django.utils import timezone

from common.tests.factories.abuse import AbuseReportFactory
from common.tests.factories.apitokens import UserTokenFactory
from common.tests.factories.extensions import RatingFactory, create_approved_version, create_version
from common.tests.factories.users import OAuthUserFactory, create_moderator, UserFactory
import users.tasks as tasks

User = get_user_model()


class TestTasks(TestCase):
    fixtures = ['dev', 'licenses']

    def create_account_data_that_can_be_deleted(self, user):
        """Create objects which don't prevent account deletion."""
        self.api_token1 = UserTokenFactory(user=user)
        self.api_token2 = UserTokenFactory(user=user)

    def create_account_data_that_cannot_be_deleted(self, user):
        """Create objects which prevent account deletion but allow anonymisation."""
        self.authored_listed_extension = create_approved_version(user=user).extension
        self.assertTrue(self.authored_listed_extension.is_listed)
        self.rating1 = RatingFactory(user=user, version=create_approved_version())
        self.rating2 = RatingFactory(user=user, version=create_approved_version())
        # Abuse reported BY this account:
        reported_extension1 = create_approved_version().extension
        reported_extension2 = create_approved_version().extension
        self.report1 = AbuseReportFactory(reporter=user, extension=reported_extension1)
        self.report2 = AbuseReportFactory(reporter=user, extension=reported_extension2)
        # Abuse reported by someone else ABOUT this account:
        self.report3 = AbuseReportFactory(user=user)
        self.report4 = AbuseReportFactory(user=user)
        self.authored_unlisted_extension = create_version(user=user).extension
        self.assertEqual(self.authored_unlisted_extension.authors.first(), user)
        self.assertFalse(self.authored_unlisted_extension.is_listed)

    def test_handle_deletion_request_anonymized(self):
        now = timezone.now()
        user = OAuthUserFactory(
            email='mail1@example.com',
            date_deletion_requested=now - timedelta(days=30),
            oauth_info__oauth_user_id=2233,
            oauth_tokens__oauth_user_id=2233,
            oauth_tokens__access_token='testaccesstoken',
            oauth_tokens__refresh_token='testrefreshtoken',
        )
        self.create_account_data_that_can_be_deleted(user)
        self.create_account_data_that_cannot_be_deleted(user)

        with self.assertLogs('users.models', level='WARNING') as log:
            tasks.handle_deletion_request.task_function(pk=user.pk)
            self.assertIn(
                f'Deleting 2 OAuth tokens with OAuth ID=2233,2233 pk={user.pk}',
                log.output[0],
            )
            self.assertIn(
                f'Deleting 1 OAuth infos with OAuth ID=2233 pk={user.pk}',
                log.output[1],
            )
            self.assertIn(f'Deleting 2 API tokens of pk={user.pk}', log.output[2])
            self.assertIn(f'Anonymized User pk={user.pk}', log.output[4])

        # user wasn't deleted but anonymised
        user.refresh_from_db()
        self.assertFalse(user.is_active)
        self.assertEqual(user.full_name, '')
        self.assertTrue(user.email.startswith('del'))
        self.assertTrue(user.email.endswith('@example.com'))
        self.assertIsNotNone(user.date_deletion_processed)

        # Check that publicly extension and version remained publicly accessible
        version = self.authored_listed_extension.latest_version
        self.authored_listed_extension.refresh_from_db()
        version.refresh_from_db()
        response = self.client.get(version.extension.get_absolute_url())
        self.assertEqual(response.status_code, 200)
        response = self.client.get(version.extension.get_versions_url())
        self.assertEqual(response.status_code, 200)
        response = self.client.get(version.get_download_url(version.files.first()))
        self.assertEqual(response.status_code, 302)
        self.assertEqual(
            response['Location'],
            f'/media/{version.files.first().original_name}'
            f'?filename=add-on-{version.extension.slug}-v{version.version}.zip',
        )
        # Check that ratings remained publicly accessible
        response = self.client.get(self.rating1.extension.get_ratings_url())
        self.assertEqual(response.status_code, 200)
        response = self.client.get(self.rating2.extension.get_ratings_url())
        self.assertEqual(response.status_code, 200)

        # Check that API tokens were deleted
        with self.assertRaises(UserTokenFactory._meta.model.DoesNotExist):
            self.api_token1.refresh_from_db()
        with self.assertRaises(UserTokenFactory._meta.model.DoesNotExist):
            self.api_token2.refresh_from_db()

        # Check that ratings remain accessible
        self.assertEqual(self.client.get(self.rating1.extension.get_ratings_url()).status_code, 200)
        self.assertEqual(self.client.get(self.rating2.extension.get_ratings_url()).status_code, 200)

        # Check that authored listed extension remains accessible
        extension_url = self.authored_listed_extension.get_absolute_url()
        self.assertEqual(self.client.get(extension_url).status_code, 200)

        # Check that reports remained accessible
        self.report1.refresh_from_db()
        self.report2.refresh_from_db()
        self.report3.refresh_from_db()
        self.report4.refresh_from_db()
        moderator = create_moderator()
        self.client.force_login(moderator)
        self.assertEqual(self.client.get(self.report1.get_absolute_url()).status_code, 200)
        self.assertEqual(self.client.get(self.report2.get_absolute_url()).status_code, 200)
        self.assertEqual(self.client.get(self.report3.get_absolute_url()).status_code, 200)
        self.assertEqual(self.client.get(self.report4.get_absolute_url()).status_code, 200)

    def test_handle_deletion_request_deleted(self):
        now = timezone.now()
        user = OAuthUserFactory(
            email='mail1@example.com',
            date_deletion_requested=now - timedelta(days=30),
            oauth_info__oauth_user_id=2233,
            oauth_tokens__oauth_user_id=2233,
            oauth_tokens__access_token='testaccesstoken',
            oauth_tokens__refresh_token='testrefreshtoken',
        )
        self.create_account_data_that_can_be_deleted(user)

        with self.assertLogs('users.models', level='WARNING') as log:
            tasks.handle_deletion_request.task_function(pk=user.pk)
            self.assertIn(f'Deleted User pk={user.pk}', log.output[-1])

        # user got deleted
        with self.assertRaises(User.DoesNotExist):
            user.refresh_from_db()

    def test_handle_deletion_request_cannot_delete_or_anonymize(self):
        now = timezone.now()
        moderator = create_moderator(date_deletion_requested=now - timedelta(days=30))
        staff = UserFactory(is_staff=True, date_deletion_requested=now - timedelta(days=30))
        superuser = UserFactory(
            is_superuser=True, is_staff=True, date_deletion_requested=now - timedelta(days=30)
        )

        for account, role in (
            (moderator, 'moderator'),
            (staff, 'staff'),
            (superuser, 'superuser'),
        ):
            with self.subTest(role=role), self.assertLogs('users', level='ERROR') as log:
                tasks.handle_deletion_request.task_function(pk=account.pk)

                self.assertIn(
                    f'User pk={account.pk} cannot be deleted due to a special role',
                    log.output[0],
                )
                account.refresh_from_db()
