# Generated by Django 4.0.6 on 2024-02-26 16:33

from django.db import migrations, models


def approve_all_reviews(apps, schema_editor):
    model = apps.get_model('ratings', 'rating')
    for ob in model.objects.all():
        # APPROVE
        ob.status = 3
        ob.save()


class Migration(migrations.Migration):

    dependencies = [
        ('ratings', '0003_alter_rating_score'),
    ]

    operations = [
        migrations.AlterField(
            model_name='rating',
            name='status',
            field=models.PositiveSmallIntegerField(choices=[(2, 'Awaiting Review'), (3, 'Approved'), (4, 'Rejected by staff')], default=3),
        ),
        migrations.RunPython(approve_all_reviews),
    ]
