from collections import defaultdict
from math import sqrt


def compute_rating_sortkey(scores):
    """Implementation of https://www.evanmiller.org/ranking-items-with-star-ratings.html

    Suggested by HugeMenace in
    https://projects.blender.org/infrastructure/extensions-website/issues/145
    """
    N = len(scores)
    K = 5
    n = defaultdict(int)
    z = 1.65
    for score in scores:
        n[score] += 1

    A = sum(k**2 * (n[k] + 1) for k in range(1, K + 1)) / (N + K)
    B = sum(k * (n[k] + 1) for k in range(1, K + 1)) / (N + K)

    return B - z * sqrt((A - B**2) / (N + K + 1))
