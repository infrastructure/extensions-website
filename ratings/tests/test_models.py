from django.test import TestCase

from common.tests.factories.extensions import RatingFactory, create_approved_version
from ratings.models import Rating


class RatingsModelTest(TestCase):
    fixtures = ['dev', 'licenses']

    def test_average_score(self):
        version = create_approved_version()
        ratings = [
            RatingFactory(
                score=5,
                status=Rating.STATUSES.APPROVED,
                text='some text',
                version=version,
            ),
            RatingFactory(
                score=4,
                status=Rating.STATUSES.APPROVED,
                text='some text',
                version=version,
            ),
            RatingFactory(
                score=3,
                status=Rating.STATUSES.APPROVED,
                text='some text',
                version=version,
            ),
        ]
        extension = version.extension
        extension.refresh_from_db()
        self.assertEqual(extension.average_score, 4)

        ratings[0].delete()
        extension.refresh_from_db()
        self.assertEqual(extension.average_score, 3.5)

        ratings[1].status = Rating.STATUSES.REJECTED
        ratings[1].save()
        extension.refresh_from_db()
        self.assertEqual(extension.average_score, 3)

        ratings[2].status = Rating.STATUSES.REJECTED
        ratings[2].save()
        extension.refresh_from_db()
        self.assertEqual(extension.average_score, 0)
