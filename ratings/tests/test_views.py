from django.test import TestCase

from common.tests.factories.extensions import RatingFactory, create_approved_version
from common.tests.factories.users import UserFactory
from ratings.models import Rating


class RatingsViewTest(TestCase):
    fixtures = ['dev', 'licenses']

    def test_get_anonymous(self):
        version = create_approved_version()
        [
            RatingFactory(
                version=version, text='this rating is rejected', status=Rating.STATUSES.REJECTED
            ),
            RatingFactory(
                version=version,
                text='this rating is awaiting review',
                status=Rating.STATUSES.AWAITING_REVIEW,
            ),
            RatingFactory(
                version=version, text='this rating is approved', status=Rating.STATUSES.APPROVED
            ),
            RatingFactory(
                version=version,
                text='this rating is also approved',
                status=Rating.STATUSES.APPROVED,
            ),
        ]

        url = version.extension.get_ratings_url()
        response = self.client.get(url)

        self.assertContains(response, '2 reviews', html=True)
        self.assertContains(response, 'this rating is also approved', html=True)
        self.assertContains(response, 'this rating is approved', html=True)
        self.assertNotContains(response, 'this rating is awaiting review', html=True)
        self.assertNotContains(response, 'this rating is rejected', html=True)
        self.assertNotContains(response, 'this rating is deleted', html=True)

    def test_get_logged_in_can_see_own_unlisted_rating(self):
        version = create_approved_version()
        user = UserFactory()
        [
            RatingFactory(
                user=user,
                version=version,
                text='this rating is awaiting review',
                status=Rating.STATUSES.AWAITING_REVIEW,
            ),
            RatingFactory(
                version=version, text='this rating is approved', status=Rating.STATUSES.APPROVED
            ),
            RatingFactory(
                version=version,
                text='this rating is also approved',
                status=Rating.STATUSES.APPROVED,
            ),
        ]

        url = version.extension.get_ratings_url()
        self.client.force_login(user)
        response = self.client.get(url)

        self.assertContains(response, '2 reviews', html=True)
        self.assertContains(response, 'this rating is also approved', html=True)
        self.assertContains(response, 'this rating is approved', html=True)
        self.assertContains(response, 'this rating is awaiting review', html=True)


class AddRatingViewTest(TestCase):
    fixtures = ['dev', 'licenses']

    def test_get_anonymous_redirects_to_login(self):
        version = create_approved_version()

        url = version.extension.get_rate_url()
        response = self.client.get(url)

        self.assertEqual(response.status_code, 302)
        self.assertTrue(response['Location'].startswith('/oauth/login'))

    def test_get_logged_in_as_maintainer_cant_rate(self):
        version = create_approved_version()

        url = version.extension.get_rate_url()
        self.client.force_login(version.extension.authors.first())
        response = self.client.get(url)

        # TODO: display a human-friendly message on the page instead of 403
        self.assertEqual(response.status_code, 403)

    def test_get_logged_in_can_rate(self):
        user = UserFactory()
        version = create_approved_version()

        url = version.extension.get_rate_url()
        self.client.force_login(user)
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Rate this add-on', html=True)

    def test_post_logged_in_validation_errors(self):
        user = UserFactory()
        version = create_approved_version()
        url = version.extension.get_rate_url()
        self.client.force_login(user)

        response = self.client.post(url)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.context['form'].errors,
            {
                'score': ['This field is required.'],
                'text': ['This field is required.'],
                'version': ['This field is required.'],
            },
        )

        response = self.client.post(url, {'score': 2.3, 'text': 'foobar', 'version': version.pk})

        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.context['form'].errors,
            {'score': ['Select a valid choice. 2.3 is not one of the available choices.']},
        )

        response = self.client.post(url, {'score': 2.3, 'text': 'foobar', 'version': 12345})

        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.context['form'].errors,
            {
                'score': ['Select a valid choice. 2.3 is not one of the available choices.'],
                'version': [
                    'Select a valid choice. That choice is not one of the available choices.',
                ],
            },
        )

    def test_post_logged_in_adds_new_rating(self):
        user = UserFactory()
        version = create_approved_version()
        extension = version.extension
        self.assertEqual(Rating.objects.count(), 0)
        self.assertEqual(extension.ratings.count(), 0)
        self.assertEqual(extension.average_score, 0)

        url = extension.get_rate_url()
        self.client.force_login(user)
        text = 'Awesome extension, absolutely glorious! 7 out of 10'
        response = self.client.post(url, {'score': 3, 'text': text, 'version': version.pk})

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['Location'], f'/add-ons/{extension.slug}/reviews/')
        self.assertEqual(Rating.objects.count(), 1)
        self.assertEqual(extension.ratings.count(), 1)
        extension.refresh_from_db()
        self.assertEqual(extension.average_score, 3.0)
        rating = Rating.objects.first()
        self.assertEqual(rating.score, 3)
        self.assertEqual(rating.text, text)

    def test_reply(self):
        version = create_approved_version()
        rating = RatingFactory(version=version, text='some text', status=Rating.STATUSES.APPROVED)

        random_user = UserFactory()
        self.client.force_login(random_user)
        response = self.client.post(rating.get_reply_url(), {'text': 'some reply'})
        self.assertEqual(response.status_code, 403)

        self.client.force_login(version.extension.authors.first())
        response = self.client.post(rating.get_reply_url(), {'text': 'some reply'})
        self.assertEqual(response.status_code, 302)
        rating.refresh_from_db()
        self.assertTrue(hasattr(rating, 'ratingreply'))
