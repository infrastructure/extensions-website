from django.urls import path, re_path, include

from . import views
from constants.base import EXTENSION_SLUGS_PATH

app_name = 'ratings'
urlpatterns = [
    re_path(
        rf'^(?P<type_slug>(?:{EXTENSION_SLUGS_PATH}))/',
        include(
            [
                path('<slug:slug>/reviews/', views.RatingsView.as_view(), name='for-extension'),
                path('<slug:slug>/reviews/new/', views.AddRatingView.as_view(), name='new'),
                path(
                    '<slug:slug>/reviews/<int:pk>/reply/',
                    views.AddRatingReplyView.as_view(),
                    name='reply',
                ),
            ]
        ),
    )
]
