import logging

from django.contrib.auth.mixins import UserPassesTestMixin, LoginRequiredMixin
from django.views.generic.edit import CreateView
from django.views.generic.list import ListView

from ratings.forms import AddRatingForm
from ratings.models import Rating, RatingReply
import extensions.views.mixins

log = logging.getLogger(__name__)


class RatingsView(extensions.views.mixins.ListedExtensionMixin, ListView):
    model = Rating
    paginate_by = 15
    queryset = Rating.objects.listed

    def _set_score_filter(self):
        score = None
        if 'score' in self.request.GET:
            try:
                score = int(self.request.GET['score'])
                self.score = next(
                    (score for _score in Rating.SCORES.values if score == _score),
                    None,
                )
            except Exception:
                pass
        self.score = score

    def get_queryset(self):
        self._set_score_filter()
        queryset = (
            super()
            .get_queryset()
            .filter(extension_id=self.extension.pk)
            .select_related(
                'extension',
                'ratingreply',
                'ratingreply__user',
                'user',
                'version',
            )
        )
        if self.score:
            queryset = queryset.filter(score=self.score)
        return queryset.distinct()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['extension'] = self.extension
        context['is_maintainer'] = self.extension.has_maintainer(self.request.user)
        context['score'] = self.score
        if self.request.user.is_authenticated:
            context['my_rating'] = Rating.get_for(self.request.user.pk, self.extension.pk)
        return context


class AddRatingView(
    extensions.views.mixins.ListedExtensionMixin,
    UserPassesTestMixin,
    LoginRequiredMixin,
    CreateView,
):
    model = Rating
    form_class = AddRatingForm

    def test_func(self) -> bool:
        # FIXME: best to display a friendly message instead of just responding with a 403
        # TODO: E.g: "you already rated this extension" or "cannot rate your own extension"
        return self.extension.can_rate(self.request.user)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['extension'] = self.extension
        return kwargs

    def form_valid(self, form):
        """Link newly created rating to latest version and current user."""
        form.instance.extension = self.extension
        form.instance.user = self.request.user
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['extension'] = self.extension
        return context

    def get_success_url(self) -> str:
        return self.extension.get_ratings_url()


class AddRatingReplyView(
    extensions.views.mixins.ListedExtensionMixin,
    UserPassesTestMixin,
    LoginRequiredMixin,
    CreateView,
):
    model = RatingReply
    fields = ('text',)

    def get_rating(self):
        return Rating.objects.filter(pk=self.kwargs['pk'], extension=self.extension).first()

    def test_func(self) -> bool:
        # Check that user is replying to a rating for their exension and this is the first reply
        rating = self.get_rating()
        return (
            rating
            and not hasattr(rating, 'ratingreply')
            and self.extension.has_maintainer(self.request.user)
        )

    def form_valid(self, form):
        form.instance.rating = self.get_rating()
        form.instance.user = self.request.user
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['extension'] = self.extension
        context['rating'] = self.get_rating()
        return context

    def get_success_url(self) -> str:
        return self.extension.get_ratings_url()
