from django.contrib.admin.apps import AdminConfig


class ExtensionsAdminConfig(AdminConfig):
    default_site = 'blender_extensions.admin.ExtensionsAdminSite'
