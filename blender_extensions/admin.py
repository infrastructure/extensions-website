from django.contrib import admin

from users.mfa_check import user_has_mfa


class ExtensionsAdminSite(admin.AdminSite):
    login_template = 'admin/blender_id_login.html'

    def has_permission(self, request):
        return super().has_permission(request) and user_has_mfa(request)
