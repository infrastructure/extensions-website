from django.contrib import admin
from django.utils.html import format_html

from notifications.models import Notification


class NotificationAdmin(admin.ModelAdmin):
    readonly_fields = (
        'recipient',
        'action',
        'email_sent',
        'processed_by_mailer_at',
        'read_at',
    )
    fields = readonly_fields
    list_display = ('actor', 'verb', 'target', 'recipient', 'view_link')
    list_display_links = ['verb']

    def get_queryset(self, request):
        return Notification.objects.all()

    def verb(self, obj):
        return obj.action.verb

    def actor(self, obj):
        return obj.action.actor

    def target(self, obj):
        return obj.action.target

    @staticmethod
    def view_link(obj) -> str:
        """Link to view the notification content in context."""
        template = '<a href="{:s}">View</a>'.format(obj.get_absolute_url())
        return format_html(template)


admin.site.register(Notification, NotificationAdmin)
