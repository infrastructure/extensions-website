# Generated by Django 4.2.11 on 2024-05-24 16:15

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('reviewers', '0009_alter_approvalactivity_type'),
    ]

    operations = [
        migrations.DeleteModel(
            name='ReviewerSubscription',
        ),
    ]
