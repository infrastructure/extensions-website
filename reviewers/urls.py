from django.urls import path

from reviewers import views

app_name = 'reviewers'
urlpatterns = [
    path('approval-queue/', views.ApprovalQueueView.as_view(), name='approval-queue'),
    path(
        'approval-queue/<slug:slug>/',
        views.ExtensionsApprovalDetailView.as_view(),
        name='approval-detail',
    ),
    path(
        'approval-queue/<slug:slug>/comment',
        views.ExtensionsApprovalFormView.as_view(),
        name='approval-comment',
    ),
    path(
        'approval-queue/<slug:slug>/activity/<int:pk>/edit/',
        views.EditMessageView.as_view(),
        name='approval-edit-message',
    ),
    path(
        'approval-queue-follow/<slug:slug>/',
        views.ExtensionsApprovalFollowView.as_view(),
        name='approval-follow',
    ),
]
