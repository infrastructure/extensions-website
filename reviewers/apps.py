from django.apps import AppConfig


class ReviewersConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'reviewers'

    def ready(self) -> None:
        from actstream import registry
        import reviewers.signals  # noqa: F401

        registry.register(self.get_model('ApprovalActivity'))
