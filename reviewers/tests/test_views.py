from datetime import datetime, timedelta, timezone

from django.test import TestCase
from django.shortcuts import reverse
from freezegun import freeze_time

from common.tests.factories.extensions import create_version
from common.tests.factories.users import UserFactory, create_moderator
from extensions.models import Extension
from files.models import File
from reviewers.models import ApprovalActivity
from users.tests.util import FakeMfaClient


class CommentsViewTest(TestCase):
    client_class = FakeMfaClient
    fixtures = ['licenses']

    def setUp(self):
        version = create_version(status=File.STATUSES.AWAITING_REVIEW)
        self.default_version = version
        ApprovalActivity(
            type=ApprovalActivity.ActivityType.AWAITING_REVIEW,
            user=version.files.first().user,
            extension=version.extension,
            message='test comment',
        ).save()

    # List of extensions under review does not require authentication
    def test_list_visibility(self):
        r = self.client.get(reverse('reviewers:approval-queue'))
        self.assertEqual(r.status_code, 200)
        self.assertEqual(len(r.context['object_list']), 1)

    # Page is visible for every extension and does not require authentication
    def test_visibility(self):
        r = self.client.get(
            reverse(
                'reviewers:approval-detail',
                kwargs={'slug': self.default_version.extension.slug},
            )
        )
        self.assertEqual(r.status_code, 200)

    def test_disabled_is_not_listed(self):
        self.default_version.extension.status = Extension.STATUSES.DISABLED
        self.default_version.extension.save()
        r = self.client.get(reverse('reviewers:approval-queue'))
        self.assertEqual(r.status_code, 200)
        self.assertEqual(len(r.context['object_list']), 0)

    def test_only_moderator_can_approve(self):
        extension = self.default_version.extension
        url = reverse('reviewers:approval-comment', args=[extension.slug])
        some_user = UserFactory()

        self.client.force_login_with_mfa(some_user)
        response = self.client.post(
            url,
            {'type': ApprovalActivity.ActivityType.APPROVED, 'message': 'test'},
        )
        self.assertEqual(response.status_code, 403)

        moderator = create_moderator(email='someone@blender.org')

        self.client.force_login(moderator)
        response = self.client.post(
            url,
            {'type': ApprovalActivity.ActivityType.APPROVED, 'message': 'test'},
        )
        # requires MFA
        self.assertEqual(response.status_code, 403)

        # decline also requries MFA
        response = self.client.post(
            url,
            {'type': ApprovalActivity.ActivityType.DECLINED, 'message': 'test'},
        )
        # requires MFA
        self.assertEqual(response.status_code, 403)

        self.client.force_login_with_mfa(moderator)
        response = self.client.post(
            url,
            {'type': ApprovalActivity.ActivityType.APPROVED, 'message': 'test'},
        )
        self.assertEqual(response.status_code, 302)

        extension.refresh_from_db()
        self.assertEqual(extension.status, extension.STATUSES.APPROVED)

    # Activity form display requires authentication

    # Display only previews and version that are 'pending review'


class ModerationTest(TestCase):
    client_class = FakeMfaClient

    def test_moderator_actions(self):
        tests = [
            {
                'status': Extension.STATUSES.AWAITING_REVIEW,
                'activity_type': ApprovalActivity.ActivityType.AWAITING_CHANGES,
                'new_status': Extension.STATUSES.AWAITING_REVIEW,
            },
            {
                'status': Extension.STATUSES.AWAITING_REVIEW,
                'activity_type': ApprovalActivity.ActivityType.APPROVED,
                'new_status': Extension.STATUSES.APPROVED,
            },
            {
                'status': Extension.STATUSES.AWAITING_REVIEW,
                'activity_type': ApprovalActivity.ActivityType.DECLINED,
                'new_status': Extension.STATUSES.DECLINED,
            },
            {
                'status': Extension.STATUSES.DECLINED,
                'activity_type': ApprovalActivity.ActivityType.AWAITING_CHANGES,
                'new_status': Extension.STATUSES.AWAITING_REVIEW,
            },
        ]
        moderator = create_moderator()
        self.client.force_login_with_mfa(moderator)

        for test in tests:
            extension = create_version(status=test['status']).extension
            url = reverse('reviewers:approval-comment', args=[extension.slug])
            response = self.client.post(
                url,
                {'type': test['activity_type'], 'message': 'test'},
            )
            self.assertEqual(response.status_code, 302)
            extension.refresh_from_db()
            self.assertEqual(extension.status, test['new_status'])

    def test_maintainer_actions(self):
        tests = [
            {
                'status': Extension.STATUSES.AWAITING_REVIEW,
                'activity_type': ApprovalActivity.ActivityType.AWAITING_REVIEW,
                'new_status': Extension.STATUSES.AWAITING_REVIEW,
                'status_code': 302,
            },
            {
                'status': Extension.STATUSES.DRAFT,
                'activity_type': ApprovalActivity.ActivityType.AWAITING_REVIEW,
                'new_status': Extension.STATUSES.AWAITING_REVIEW,
                'status_code': 302,
            },
            {
                'status': Extension.STATUSES.APPROVED,
                'activity_type': ApprovalActivity.ActivityType.AWAITING_REVIEW,
                'new_status': Extension.STATUSES.APPROVED,
                'status_code': 403,
            },
            {
                'status': Extension.STATUSES.DECLINED,
                'activity_type': ApprovalActivity.ActivityType.AWAITING_REVIEW,
                'new_status': Extension.STATUSES.DECLINED,
                'status_code': 403,
            },
            {
                'status': Extension.STATUSES.DECLINED,
                'activity_type': ApprovalActivity.ActivityType.COMMENT,
                'new_status': Extension.STATUSES.DECLINED,
                'status_code': 302,
            },
        ]

        for test in tests:
            extension = create_version(status=test['status']).extension
            self.client.force_login(extension.authors.first())
            url = reverse('reviewers:approval-comment', args=[extension.slug])
            response = self.client.post(
                url,
                {'type': test['activity_type'], 'message': 'test'},
            )
            self.assertEqual(response.status_code, test['status_code'])
            extension.refresh_from_db()
            self.assertEqual(extension.status, test['new_status'])


class QueueOrderTest(TestCase):
    fixtures = ['licenses']

    def test_order(self):
        extensions = []
        user = UserFactory()
        for i in range(5):
            version = create_version(status=File.STATUSES.AWAITING_REVIEW)
            extension = version.extension
            extensions.append(extension)

        # to control the distance between timestamps
        starting_datetime = datetime(2020, 12, 31, 23, 2, 3, tzinfo=timezone.utc)
        delta_s = 0
        with freeze_time(starting_datetime + timedelta(seconds=delta_s)):
            ApprovalActivity(
                type=ApprovalActivity.ActivityType.AWAITING_REVIEW,
                user=user,
                extension=extensions[0],
                message='test comment',
            ).save()

        delta_s += 1
        with freeze_time(starting_datetime + timedelta(seconds=delta_s)):
            ApprovalActivity(
                type=ApprovalActivity.ActivityType.AWAITING_CHANGES,
                user=user,
                extension=extensions[1],
                message='test comment',
            ).save()

        delta_s += 1
        with freeze_time(starting_datetime + timedelta(seconds=delta_s)):
            ApprovalActivity(
                type=ApprovalActivity.ActivityType.AWAITING_CHANGES,
                user=user,
                extension=extensions[2],
                message='test comment',
            ).save()

        apr = None
        delta_s += 1
        with freeze_time(starting_datetime + timedelta(seconds=delta_s)):
            apr = ApprovalActivity(
                date_created=starting_datetime + timedelta(seconds=delta_s),
                type=ApprovalActivity.ActivityType.APPROVED,
                user=user,
                extension=extensions[3],
                message='test comment',
            )
            apr.save()
        delta_s += 1
        with freeze_time(starting_datetime + timedelta(seconds=delta_s)):
            ApprovalActivity(
                date_created=starting_datetime + timedelta(seconds=delta_s),
                type=ApprovalActivity.ActivityType.COMMENT,
                user=user,
                extension=extensions[3],
                message='test comment',
            ).save()

        delta_s += 1
        with freeze_time(starting_datetime + timedelta(seconds=delta_s)):
            ApprovalActivity(
                date_created=starting_datetime + timedelta(seconds=delta_s),
                type=ApprovalActivity.ActivityType.COMMENT,
                user=user,
                extension=extensions[4],
                message='test comment',
            ).save()

        response = self.client.get(reverse('reviewers:approval-queue'))
        queue = response.context['object_list']

        # extensions[4] doesn't have meaningful activity
        self.assertEqual(len(queue), 4)

        # ordered by category
        self.assertEqual(queue[0].extension, extensions[0])
        # and within category by recent timestamp
        self.assertEqual(queue[1].extension, extensions[2])
        self.assertEqual(queue[2].extension, extensions[1])
        self.assertEqual(queue[3].extension, extensions[3])

        # counts are correct
        self.assertEqual(queue[0].activity_count, 1)
        self.assertEqual(queue[1].activity_count, 1)
        self.assertEqual(queue[2].activity_count, 1)
        self.assertEqual(queue[3].activity_count, 2)

        # latest_activity doesn't account for non-meaningful activity
        self.assertEqual(queue[3].latest_activity, apr)


class QueueFilterTest(TestCase):
    fixtures = ['licenses']

    def test_order(self):
        extensions = []
        user = UserFactory()
        for i in range(10):
            version = create_version(status=File.STATUSES.AWAITING_REVIEW)
            extension = version.extension
            extensions.append(extension)

        n_awc = 4
        n_awr = 3
        n_apr = 2
        n_dcl = 1

        for i in range(n_awc):
            ApprovalActivity(
                type=ApprovalActivity.ActivityType.AWAITING_CHANGES,
                user=user,
                extension=extensions[i],
                message='test comment',
            ).save()

        for i in range(n_awr):
            ApprovalActivity(
                type=ApprovalActivity.ActivityType.AWAITING_REVIEW,
                user=user,
                extension=extensions[i + n_awc],
                message='test comment',
            ).save()

        for i in range(n_apr):
            ApprovalActivity(
                type=ApprovalActivity.ActivityType.APPROVED,
                user=user,
                extension=extensions[i + n_awc + n_awr],
                message='test comment',
            ).save()

        for i in range(n_dcl):
            ApprovalActivity(
                type=ApprovalActivity.ActivityType.DECLINED,
                user=user,
                extension=extensions[i + n_awc + n_awr + n_apr],
                message='test comment',
            ).save()

        all_response = self.client.get(reverse('reviewers:approval-queue') + '?filter_by=all')
        awr_response = self.client.get(
            reverse('reviewers:approval-queue') + '?filter_by=awaiting-review'
        )
        awc_response = self.client.get(
            reverse('reviewers:approval-queue') + '?filter_by=awaiting-changes'
        )
        apr_response = self.client.get(reverse('reviewers:approval-queue') + '?filter_by=approved')
        dcl_response = self.client.get(reverse('reviewers:approval-queue') + '?filter_by=declined')

        all_queue = all_response.context['object_list']
        awc_queue = awc_response.context['object_list']
        awr_queue = awr_response.context['object_list']
        apr_queue = apr_response.context['object_list']
        dcl_queue = dcl_response.context['object_list']

        # Ensure that each queue has the correct length
        self.assertEqual(len(all_queue), 10)
        self.assertEqual(len(awc_queue), n_awc)
        self.assertEqual(len(awr_queue), n_awr)
        self.assertEqual(len(apr_queue), n_apr)
        self.assertEqual(len(dcl_queue), n_dcl)

        # Ensure that each queue only contains the correct type of activity
        for item in awc_queue:
            self.assertEqual(
                item.latest_activity.type, ApprovalActivity.ActivityType.AWAITING_CHANGES
            )
        for item in awr_queue:
            self.assertEqual(
                item.latest_activity.type, ApprovalActivity.ActivityType.AWAITING_REVIEW
            )
        for item in apr_queue:
            self.assertEqual(item.latest_activity.type, ApprovalActivity.ActivityType.APPROVED)
        for item in dcl_queue:
            self.assertEqual(item.latest_activity.type, ApprovalActivity.ActivityType.DECLINED)
