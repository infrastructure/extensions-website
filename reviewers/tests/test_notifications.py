from django.test import TestCase
from django.urls import reverse

from common.tests.factories.extensions import create_version
from common.tests.factories.users import UserFactory
from notifications.models import Notification
from reviewers.models import ApprovalActivity


class TestNotifications(TestCase):
    def test_unsubscribe(self):
        version = create_version()
        extension = version.extension
        some_user = UserFactory()
        some_user2 = UserFactory()

        self.client.force_login(some_user)
        url = reverse('reviewers:approval-comment', args=[extension.slug])
        self.client.post(url, {'type': ApprovalActivity.ActivityType.COMMENT, 'message': 'lala'})

        notification_nr = Notification.objects.filter(recipient=some_user).count()
        self.client.force_login(some_user2)
        url = reverse('reviewers:approval-comment', args=[extension.slug])
        self.client.post(url, {'type': ApprovalActivity.ActivityType.COMMENT, 'message': 'lala2'})
        new_notification_nr = Notification.objects.filter(recipient=some_user).count()
        self.assertEqual(new_notification_nr, notification_nr + 1)

        # unsubscribe and see what happens
        self.client.force_login(some_user)
        url = reverse('reviewers:approval-follow', args=[extension.slug])
        self.client.post(url, {'follow': ''})

        self.client.force_login(some_user2)
        url = reverse('reviewers:approval-comment', args=[extension.slug])
        self.client.post(url, {'type': ApprovalActivity.ActivityType.COMMENT, 'message': 'lala3'})

        new_notification_nr2 = Notification.objects.filter(recipient=some_user).count()
        self.assertEqual(new_notification_nr2, new_notification_nr)

        # subscribe back
        self.client.force_login(some_user)
        url = reverse('reviewers:approval-follow', args=[extension.slug])
        self.client.post(url, {'follow': '1'})

        self.client.force_login(some_user2)
        url = reverse('reviewers:approval-comment', args=[extension.slug])
        self.client.post(url, {'type': ApprovalActivity.ActivityType.COMMENT, 'message': 'lala4'})

        new_notification_nr3 = Notification.objects.filter(recipient=some_user).count()
        self.assertEqual(new_notification_nr3, new_notification_nr2 + 1)

    def test_preferences(self):
        version = create_version()
        extension = version.extension
        some_user = UserFactory()
        some_user2 = UserFactory()

        self.client.force_login(some_user)
        url = reverse('reviewers:approval-comment', args=[extension.slug])
        self.client.post(url, {'type': ApprovalActivity.ActivityType.COMMENT, 'message': 'lala'})

        notification_nr = Notification.objects.filter(recipient=some_user).count()
        self.client.force_login(some_user2)
        url = reverse('reviewers:approval-comment', args=[extension.slug])
        self.client.post(url, {'type': ApprovalActivity.ActivityType.COMMENT, 'message': 'lala2'})
        new_notification_nr = Notification.objects.filter(recipient=some_user).count()
        self.assertEqual(new_notification_nr, notification_nr + 1)

        # unsubscribe from all types and see what happens
        self.client.force_login(some_user)
        url = reverse('users:save-notification-preferences')
        self.client.post(url, {})

        self.client.force_login(some_user2)
        url = reverse('reviewers:approval-comment', args=[extension.slug])
        self.client.post(url, {'type': ApprovalActivity.ActivityType.COMMENT, 'message': 'lala3'})

        new_notification_nr2 = Notification.objects.filter(recipient=some_user).count()
        self.assertEqual(new_notification_nr2, new_notification_nr)

        # subscribe back
        self.client.force_login(some_user)
        url = reverse('users:save-notification-preferences')
        self.client.post(url, {'notification_preferences': 'commented'})

        self.client.force_login(some_user2)
        url = reverse('reviewers:approval-comment', args=[extension.slug])
        self.client.post(url, {'type': ApprovalActivity.ActivityType.COMMENT, 'message': 'lala4'})

        new_notification_nr3 = Notification.objects.filter(recipient=some_user).count()
        self.assertEqual(new_notification_nr3, new_notification_nr2 + 1)
