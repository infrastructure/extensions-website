from django import forms
import reviewers.models


class CommentForm(forms.ModelForm):
    class Meta:
        model = reviewers.models.ApprovalActivity
        fields = ('message', 'type')
        widgets = {
            'message': forms.Textarea(attrs={'placeholder': 'Add your comment here...'}),
        }


class SelectWidget(forms.Select):
    disabled_choices = []

    def __init__(self, *args, **kwargs):
        disabled_choices = kwargs.pop('disabled_choices', [])
        super().__init__(*args, **kwargs)
        self.disabled_choices = disabled_choices

    def create_option(self, name, value, *args, **kwargs):
        option = super().create_option(name, value, *args, **kwargs)
        if value in self.disabled_choices:
            option['attrs'].update({'disabled': 'disabled'})
        return option
