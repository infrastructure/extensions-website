import logging

from actstream.actions import follow, is_following, unfollow
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.core.exceptions import PermissionDenied
from django.db import transaction
from django.views.generic import DetailView, FormView
from django.views.generic.edit import UpdateView
from django.views.generic.list import ListView
from django.shortcuts import redirect, reverse
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
import django.forms

from collections import OrderedDict
from constants.activity import Flag
from extensions.models import Extension
from reviewers.forms import CommentForm, SelectWidget
from reviewers.models import ApprovalActivity, ApprovalQueue
from users.mfa_check import user_has_mfa

log = logging.getLogger(__name__)

activity_type2test_func = {
    ApprovalActivity.ActivityType.APPROVED: (
        lambda user, extension: (
            (user.is_moderator or user.is_superuser)
            and extension.can_set_status(extension.STATUSES.APPROVED)
        )
    ),
    ApprovalActivity.ActivityType.AWAITING_CHANGES: (
        lambda user, _: user.is_moderator or user.is_superuser
    ),
    ApprovalActivity.ActivityType.AWAITING_REVIEW: (
        lambda user, extension: (
            (
                extension.has_maintainer(user)
                and extension.status in {
                    extension.STATUSES.DRAFT, extension.STATUSES.AWAITING_REVIEW,
                }
            )
            or (
                (user.is_moderator or user.is_superuser)
                and extension.can_set_status(extension.STATUSES.AWAITING_REVIEW)
            )
        )
    ),
    ApprovalActivity.ActivityType.COMMENT: (
        lambda _, __: True
    ),
    ApprovalActivity.ActivityType.DECLINED: (
        lambda user, extension: (
            (user.is_moderator or user.is_superuser)
            and extension.can_set_status(extension.STATUSES.DECLINED)
        )
    ),
}


class ApprovalQueueView(ListView):
    model = Extension
    template_name = 'reviewers/extensions_review_list.html'
    paginate_by = 50
    default_filter_by = 'all'
    filter_by_options = OrderedDict(
        [
            ('all', _('All')),
            ('awaiting-review', _('Awaiting Review')),
            ('awaiting-changes', _('Awaiting Changes')),
            ('approved', _('Approved')),
            ('declined', _('Declined')),
        ]
    )

    def _get_filter_by(self):
        filter_by = self.request.GET.get('filter_by', self.default_filter_by)
        if filter_by not in self.filter_by_options:
            filter_by = self.default_filter_by
        return filter_by

    def get_queryset(self):
        queryset = (
            ApprovalQueue.objects.select_related(
                'extension',
                'latest_activity',
            )
            .prefetch_related(
                'extension__authors',
                'extension__icon',
                'extension__versions',
                'extension__versions__files',
                'extension__versions__files__validation',
            )
            .exclude(
                extension__status__in=[
                    Extension.STATUSES.DISABLED,
                    Extension.STATUSES.DISABLED_BY_AUTHOR,
                ]
            )
            .order_by('-sortkey')
        )

        latest_activity_type = None
        filter_by = self._get_filter_by()

        match filter_by:
            case 'awaiting-review':
                latest_activity_type = ApprovalActivity.ActivityType.AWAITING_REVIEW
            case 'awaiting-changes':
                latest_activity_type = ApprovalActivity.ActivityType.AWAITING_CHANGES
            case 'approved':
                latest_activity_type = ApprovalActivity.ActivityType.APPROVED
            case 'declined':
                latest_activity_type = ApprovalActivity.ActivityType.DECLINED

        if latest_activity_type is not None:
            queryset = queryset.filter(
                latest_activity__type=latest_activity_type,
            )

        return queryset

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        filter_by = self._get_filter_by()
        context['filter_by'] = filter_by
        context['filter_by_option_name'] = self.filter_by_options.get(filter_by)
        context['filter_by_options'] = self.filter_by_options

        return context


class ExtensionsApprovalDetailView(DetailView):
    model = Extension

    template_name = 'reviewers/extensions_review_detail.html'

    def get_queryset(self):
        return self.model.objects.exclude(
            status__in=[
                Extension.STATUSES.DISABLED,
                Extension.STATUSES.DISABLED_BY_AUTHOR,
            ],
        ).prefetch_related(
            'authors',
            'latest_version__files',
            'latest_version__files__validation',
            'latest_version__permissions',
            'latest_version__platforms',
            'latest_version__tags',
            'preview_set',
            'preview_set__file',
        ).all()

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        review_activity = (
            self.object.review_activity.select_related('user')
            .prefetch_related('user__groups')
            .order_by('date_created')
            .all()
        )
        maintainers = self.object.all_maintainers()
        for activity in review_activity:
            if activity.user in maintainers:
                activity.user_is_maintainer = True

        ctx['review_activity'] = review_activity
        ctx['status_change_types'] = ApprovalActivity.STATUS_CHANGE_TYPES

        if self.request.user.is_authenticated:
            initial = {}
            if 'report_compatibility_issue' in self.request.GET:
                version = self.request.GET.get('version')
                initial['message'] = (
                    f'Compatibility range for version {version} is outdated\n'
                    'Platform: ...\n'
                    'Version of Blender where the add-on is **no longer** working: ?????'
                )
            form = ctx['comment_form'] = CommentForm(initial=initial)
            user = self.request.user
            if self.object.has_maintainer(user):
                ctx['is_maintainer'] = self.object.has_maintainer(user)

            filtered_activity_types = [
                t for t in activity_type2test_func
                if activity_type2test_func[t](user, self.object)
            ]
            choices = list(
                filter(
                    lambda c: c[0] in filtered_activity_types, ApprovalActivity.ActivityType.choices
                )
            )
            if len(choices) == 1:
                form.fields['type'].widget = django.forms.HiddenInput()
            else:
                disabled_choices = []
                if not user_has_mfa(self.request):
                    disabled_choices.append(ApprovalActivity.ActivityType.APPROVED)
                    disabled_choices.append(ApprovalActivity.ActivityType.DECLINED)
                form.fields['type'].widget = SelectWidget(
                    choices=choices,
                    disabled_choices=disabled_choices,
                )
            ctx['user_is_following'] = is_following(user, self.object, flag=Flag.REVIEWER)
        return ctx


class ExtensionsApprovalFormView(LoginRequiredMixin, FormView):
    # TODO merge this view with DetailView
    # there is no apparent benefit in keeping this form as its own view,
    # but it does complicate error handling
    form_class = CommentForm
    http_method_names = ['post']
    template_name = 'reviewers/extensions_review_detail.html'

    def get_success_url(self):
        return reverse('reviewers:approval-detail', kwargs={'slug': self.kwargs['slug']})

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx['comment_form'] = ctx['form']
        ctx['extension'] = Extension.objects.get(slug=self.kwargs['slug'])
        return ctx

    @transaction.atomic
    def form_valid(self, form):
        form.instance.user = self.request.user
        extension = form.instance.extension = Extension.objects.get(slug=self.kwargs['slug'])
        activity_type = form.cleaned_data['type']
        if test_func := activity_type2test_func.get(activity_type):
            if not test_func(self.request.user, extension):
                raise PermissionDenied(
                    f'{self.request.user} is not allowed to {activity_type} extension {extension}'
                )
        else:
            raise PermissionDenied(
                f'{activity_type} is not supported by this interface'
            )

        if activity_type == ApprovalActivity.ActivityType.APPROVED:
            if not user_has_mfa(self.request):
                raise PermissionDenied('MFA required')
            extension.approve()
        if activity_type == ApprovalActivity.ActivityType.DECLINED:
            if not user_has_mfa(self.request):
                raise PermissionDenied('MFA required')
            extension.decline()
        if activity_type in (
            ApprovalActivity.ActivityType.AWAITING_CHANGES,
            ApprovalActivity.ActivityType.AWAITING_REVIEW,
        ):
            extension.status = Extension.STATUSES.AWAITING_REVIEW
            extension.save(update_fields=['status', 'date_status_changed'])

        form.save()
        return super().form_valid(form)


class EditMessageView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    fields = ['message']
    model = ApprovalActivity
    template_name = 'reviewers/edit_message.html'

    def get_success_url(self):
        activity = self.get_object()
        extension = activity.extension
        return extension.get_review_url() + f'#activity-{activity.pk}'

    def test_func(self):
        return self.get_object().user == self.request.user and self.request.user.is_moderator

    def form_valid(self, form):
        form.instance.date_edited = timezone.now()
        return super().form_valid(form)


class ExtensionsApprovalFollowView(LoginRequiredMixin, DetailView):
    model = Extension

    def post(self, request, *args, **kwargs):
        extension = self.get_object()
        user = request.user
        if request.POST.get('follow'):
            follow(user, extension, send_action=False, flag=Flag.REVIEWER)
        else:
            unfollow(user, extension, send_action=False, flag=Flag.REVIEWER)
        return redirect(extension.get_review_url() + '#activity')
